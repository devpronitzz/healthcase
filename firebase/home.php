<?php

require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase_credentials.json');

$firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    ->withDatabaseUri('https://projects-a6962.firebaseio.com')
    ->create();

$database = $firebase->getDatabase();

//get data
// $newPost = $database->getReference('healthcare_login')
//            			->orderByChild('email_id','password')
//    					->equalTo('f1@f.com','111111');
// $snapshot = $newPost->getSnapshot();
// $k1 = $snapshot->getValue();

// print_r($k1);

// put data 
// $uid = '0';
// $postData = [
//     'call_status' => '0'
// ];

// $updates = [
//     //'doctor_signup/'.$newPostKey => $postData
//      'doctor_signup/0/call_status' => '2'
//      ];

// $res = $database->getReference() // this is the root reference
//    ->update($updates);
// print_r($res);


// post data
$postData = [
'cancel_took_doctor' => "56",
'cancel_took_patient' => "67",
'id' => "3"
];
$postRef = $database->getReference('cancel_took')->push($postData);
