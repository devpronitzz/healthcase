<?php

namespace PackageVersions;

/**
 * This class is generated by ocramius/package-versions, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 */
final class Versions
{
    const VERSIONS = array (
  'fig/http-message-util' => '1.1.2@20b2c280cb6914b7b83089720df44e490f4b42f0',
  'firebase/php-jwt' => 'v5.0.0@9984a4d3a32ae7673d6971ea00bae9d0a1abba0e',
  'google/auth' => 'v1.2.1@da0062d279c9459350808a4fb63dbc08b90d6b90',
  'guzzlehttp/guzzle' => '6.3.0@f4db5a78a5ea468d4831de7f0bf9d9415e348699',
  'guzzlehttp/promises' => 'v1.3.1@a59da6cf61d80060647ff4d3eb2c03a2bc694646',
  'guzzlehttp/psr7' => '1.4.2@f5b8a8512e2b58b0071a7280e39f14f72e05d87c',
  'kreait/firebase-tokens' => '1.7.1@85b8cdf69bccf0d3fd89cd4b65714696d0eff7e2',
  'lcobucci/jwt' => '3.2.2@0b5930be73582369e10c4d4bb7a12bac927a203c',
  'mtdowling/jmespath.php' => '2.4.0@adcc9531682cf87dfda21e1fd5d0e7a41d292fac',
  'psr/cache' => '1.0.1@d11b50ad223250cf17b86e38383413f5a6764bf8',
  'psr/http-message' => '1.0.1@f6561bf28d520154e4b0ec72be95418abe6d9363',
  'psr/simple-cache' => '1.0.0@753fa598e8f3b9966c886fe13f370baa45ef0e24',
  'composer/semver' => '1.4.2@c7cb9a2095a074d131b65a8a0cd294479d785573',
  'doctrine/annotations' => 'v1.4.0@54cacc9b81758b14e3ce750f205a393d52339e97',
  'doctrine/instantiator' => '1.0.5@8e884e78f9f0eb1329e445619e04456e64d8051d',
  'doctrine/lexer' => 'v1.0.1@83893c552fd2045dd78aef794c31e694c37c0b8c',
  'friendsofphp/php-cs-fixer' => 'v2.10.2@74e4682a4073bc8bc2d4ff2b30a4873ac76cc1f1',
  'gecko-packages/gecko-php-unit' => 'v3.1@dbb18b292cfa14444d2e6d15202b9dcf5ab8365e',
  'jean85/pretty-package-versions' => '1.1@d457344b6a035ef99236bdda4729ad7eeb233f54',
  'myclabs/deep-copy' => '1.7.0@3b8a3a99ba1f6a3952ac2747d989303cbd6b7a3e',
  'nette/bootstrap' => 'v2.4.5@804925787764d708a7782ea0d9382a310bb21968',
  'nette/di' => 'v2.4.10@a4b3be935b755f23aebea1ce33d7e3c832cdff98',
  'nette/finder' => 'v2.4.1@4d43a66d072c57d585bf08a3ef68d3587f7e9547',
  'nette/neon' => 'v2.4.2@9eacd50553b26b53a3977bfb2fea2166d4331622',
  'nette/php-generator' => 'v3.0.2@1652635d312a8db4291b16f3ebf87cb1a15a6257',
  'nette/robot-loader' => 'v3.0.3@92d4b40b49d5e2d9e37fc736bbcebe6da55fa44a',
  'nette/utils' => 'v2.4.9@1e08eb4c9d26ae5aedced8260e8b4ed951ee4aa6',
  'nikic/php-parser' => 'v3.1.4@e57b3a09784f846411aa7ed664eedb73e3399078',
  'ocramius/package-versions' => '1.2.0@ad8a245decad4897cc6b432743913dad0d69753c',
  'paragonie/random_compat' => 'v2.0.11@5da4d3c796c275c55f057af5a643ae297d96b4d8',
  'phar-io/manifest' => '1.0.1@2df402786ab5368a0169091f61a7c1e0eb6852d0',
  'phar-io/version' => '1.0.1@a70c0ced4be299a63d32fa96d9281d03e94041df',
  'php-cs-fixer/diff' => 'v1.2.1@b95b8c02c58670b15612cfc60873f3f7f5290484',
  'phpdocumentor/reflection-common' => '1.0.1@21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6',
  'phpdocumentor/reflection-docblock' => '4.3.0@94fd0001232e47129dd3504189fa1c7225010d08',
  'phpdocumentor/type-resolver' => '0.4.0@9c977708995954784726e25d0cd1dddf4e65b0f7',
  'phpspec/prophecy' => '1.7.3@e4ed002c67da8eceb0eb8ddb8b3847bb53c5c2bf',
  'phpstan/phpdoc-parser' => '0.2@02f909f134fe06f0cd4790d8627ee24efbe84d6a',
  'phpstan/phpstan' => '0.9.2@e59541bcc7cac9b35ca54db6365bf377baf4a488',
  'phpstan/phpstan-phpunit' => '0.9.4@852411f841a37aeca2fa5af0002b0272c485c9bf',
  'phpunit/php-code-coverage' => '5.3.0@661f34d0bd3f1a7225ef491a70a020ad23a057a1',
  'phpunit/php-file-iterator' => '1.4.5@730b01bc3e867237eaac355e06a36b85dd93a8b4',
  'phpunit/php-text-template' => '1.2.1@31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
  'phpunit/php-timer' => '1.0.9@3dcf38ca72b158baf0bc245e9184d3fdffa9c46f',
  'phpunit/php-token-stream' => '2.0.2@791198a2c6254db10131eecfe8c06670700904db',
  'phpunit/phpunit' => '6.5.6@3330ef26ade05359d006041316ed0fa9e8e3cefe',
  'phpunit/phpunit-mock-objects' => '5.0.6@33fd41a76e746b8fa96d00b49a23dadfa8334cdf',
  'psr/log' => '1.0.2@4ebe3a8bf773a19edfe0a84b6585ba3d401b724d',
  'sebastian/code-unit-reverse-lookup' => '1.0.1@4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
  'sebastian/comparator' => '2.1.3@34369daee48eafb2651bea869b4b15d75ccc35f9',
  'sebastian/diff' => '2.0.1@347c1d8b49c5c3ee30c7040ea6fc446790e6bddd',
  'sebastian/environment' => '3.1.0@cd0871b3975fb7fc44d11314fd1ee20925fce4f5',
  'sebastian/exporter' => '3.1.0@234199f4528de6d12aaa58b612e98f7d36adb937',
  'sebastian/global-state' => '2.0.0@e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
  'sebastian/object-enumerator' => '3.0.3@7cfd9e65d11ffb5af41198476395774d4c8a84c5',
  'sebastian/object-reflector' => '1.1.1@773f97c67f28de00d397be301821b06708fca0be',
  'sebastian/recursion-context' => '3.0.0@5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
  'sebastian/resource-operations' => '1.0.0@ce990bb21759f94aeafd30209e8cfcdfa8bc3f52',
  'sebastian/version' => '2.0.1@99732be0ddb3361e16ad77b68ba41efc8e979019',
  'symfony/console' => 'v3.3.6@b0878233cb5c4391347e5495089c7af11b8e6201',
  'symfony/debug' => 'v3.3.6@7c13ae8ce1e2adbbd574fc39de7be498e1284e13',
  'symfony/event-dispatcher' => 'v3.3.6@67535f1e3fd662bdc68d7ba317c93eecd973617e',
  'symfony/filesystem' => 'v3.3.6@427987eb4eed764c3b6e38d52a0f87989e010676',
  'symfony/finder' => 'v3.3.6@baea7f66d30854ad32988c11a09d7ffd485810c4',
  'symfony/options-resolver' => 'v3.3.6@ff48982d295bcac1fd861f934f041ebc73ae40f0',
  'symfony/polyfill-mbstring' => 'v1.7.0@78be803ce01e55d3491c1397cf1c64beb9c1b63b',
  'symfony/polyfill-php70' => 'v1.7.0@3532bfcd8f933a7816f3a0a59682fc404776600f',
  'symfony/polyfill-php72' => 'v1.7.0@8eca20c8a369e069d4f4c2ac9895144112867422',
  'symfony/process' => 'v3.3.6@07432804942b9f6dd7b7377faf9920af5f95d70a',
  'symfony/stopwatch' => 'v3.3.6@602a15299dc01556013b07167d4f5d3a60e90d15',
  'theseer/tokenizer' => '1.1.0@cb2f008f3f05af2893a87208fe6a6c4985483f8b',
  'webmozart/assert' => '1.3.0@0df1908962e7a3071564e857d86874dad1ef204a',
  'kreait/firebase-php' => 'No version set (parsed as 1.0.0)@',
);

    private function __construct()
    {
    }

    /**
     * @throws \OutOfBoundsException if a version cannot be located
     */
    public static function getVersion(string $packageName) : string
    {
        if (! isset(self::VERSIONS[$packageName])) {
            throw new \OutOfBoundsException(
                'Required package "' . $packageName . '" is not installed: cannot detect its version'
            );
        }

        return self::VERSIONS[$packageName];
    }
}
