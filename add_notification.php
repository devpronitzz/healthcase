 <?php 
// turn on output buffering 
ob_start();

    header('Content-Type: application/json; charset=utf-8');
	header('Access-Control-Allow-Origin: *');
	error_reporting( E_ALL & ~E_DEPRECATED & ~E_NOTICE );
 
include_once 'connection/db.php';
include_once 'publicClass/responseClass.php'; 
include("android_notification.php");



    define('DB_DRIVER', 'mysql');
	define("DB_HOST", $Mdb_host);
	define("DB_USER", $Mdb_username);
	define("DB_PASSWORD", $Mdb_password);
	define("DB_DATABASE", $Mdb_name);

// basic options for PDO 
$dboptions = array(
    PDO::ATTR_PERSISTENT => FALSE,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
);
 
//connect with the server
try {

$DB = new PDO(DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD, $dboptions);



$job_id = $_POST['job_id'];
$agent_id = $_POST['agent_id'];
$user_id = $_POST['user_id'];
$sender = $_POST['sender'];
//$receiver = $_POST['receiver'];
$type = $_POST['type'];
$comment1 = $_POST['comment'];
$comment = htmlentities($comment1, ENT_QUOTES, 'UTF-8');


if(isset($_POST)) {
			
			
			   $sender1 = "User";
			   $Rec = "Agent";
			   
			   $sqlRec = "SELECT * FROM tbl_agent WHERE agent_id = '$agent_id'";
			   $stmtRec = $DB->prepare($sqlRec);
			   $stmtRec->execute();
			   $resultRec = $stmtRec->fetch();
			   
			   $sqlSen = "SELECT * FROM tbl_user_register WHERE id = '$user_id'";
			   $stmtSen = $DB->prepare($sqlSen);
			   $stmtSen->execute();
			   $resultSen = $stmtSen->fetch();
			   
			   $Sender_Name = $resultSen['name'];
			   
			   $DeviceId = $resultRec['device_id'];
			   $DeviceType = $resultRec['device_type'];
			   
			   
			$date_time = date("Y-m-d h:i:sa");
			
			$sql ="INSERT INTO `tbl_chat`(`job_id`, `agent_id`, `user_id`, `sender`, `receiver`, `type`, `image`, `image_name`, `comment`, `date_time`) VALUES(:job_id,:agent_id,:user_id,:sender,:receiver,:type,:image,:image_name,:comment,:date_time)"; 
			
			$statement = $DB->prepare($sql);
            $statement->bindValue(":job_id", $job_id);
			$statement->bindValue(":agent_id", $agent_id);
			$statement->bindValue(":user_id", $user_id);
			$statement->bindValue(":sender", $sender1);
			$statement->bindValue(":receiver", $Rec);	
			$statement->bindValue(":type", $type);	
			$statement->bindValue(":image", $array_full);	
			$statement->bindValue(":image_name", $array_full);	
			$statement->bindValue(":comment", $comment);	
			$statement->bindValue(":date_time", $date_time);	
			$count = $statement->execute();

			

			
			//$DeviceId = "1955645a3b0c4a1c8f0161d41f3b13da9b42610111f586454d552a19b47090eb";
			
			if($DeviceType == "ios") {

				if($DeviceId != "") {
					push_notification($DeviceId,$Sender_Name,$comment1,$sender1);
				}

			}

			else{

				// echo "Dev_Id : ".$DeviceId;
				// echo "Sender : ".$Sender_Name;
				// echo "Comment : ".$comment1;
				// echo "Sender :".$sender1;

				push_notification1($DeviceId,$Sender_Name,$comment1,$sender1);
			}
			
			$rsFormatted['success'] = 1;
	}
else {
		$rsFormatted['success'] = 0;
	 }
	 
} 

catch (Exception $ex) {
   //$rsFormatted['success'] = 0;
}

echo json_encode($rsFormatted, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);


?>


