<?php	
session_start();
	include('classes/firebaseclass.php');
	header('Content-type: application/json; charset=utf-8');
	$CustomHeaders = apache_request_headers();
	
	$getlessiondetail=array();
	
	if(!empty($_POST['email_id']) and !empty($_POST['password']) and isset($_POST['token']) and !empty($_POST['token']))
	{
		$emailID = $_POST['email_id'];
		$password1 = $_POST['password'];
		$latitude = $_POST['lat'];
		$logitude = $_POST['log'];
		$password = urldecode($password1);
		$token = $_POST['token'];
		$type = $_POST['type'];
		
		if(($latitude !== "" && $latitude !== "0.0") && ($logitude !== "" && $logitude !== "0.0")){
			
		$dataObj = new UserClass();
			$result2 = $dataObj->check_login_details($emailID,$password, $type);
			if(count($result2) == 1)
			{
				//print_r($result2);
				foreach($result2 as $key=>$result)
				{
					
					// print_r($key);
					// 						die("debug stop");

					$restype = $result['type'];
					
					//echo $restype;
					if($restype == 'D')
						{
							
						$updatetoken = $dataObj->updatedoctortoken($emailID,$password,$token,$latitude,$logitude,$key);
							$result = $dataObj ->getdoctordetaildetails($emailID,$password);

							if(count($result) > 0)
							{
				
								$getlessiondetail[] = $result;
								ResponseClass::successResponseInArray("AllDetails",$getlessiondetail,"1","Successfully Response","True");

							}
							else
							{
								ResponseClass::ResponseMessage("3","Does Not Match!","False");
							}
						}
						elseif($restype == 'P')
						{
							$updatetoken = $dataObj ->updatepatienttoken($emailID,$password,$token,$latitude,$logitude);
							$result = $dataObj ->getpatient_details($emailID,$password);
							if(count($result) > 0)
							{
								$getlessiondetail[] = $result;
								ResponseClass::successResponseInArray("AllDetails",$getlessiondetail,"1","Successfully Response","True");
							}
							else
							{
								ResponseClass::ResponseMessage("4","Does Not Match!","False");
							}
						}
						else
						{
							$updatetoken = $dataObj->updatepharmacytoken($emailID,$password,$token,$latitude,$logitude);
							$result = $dataObj ->getpharmacy_details($emailID,$password);
							if(count($result) > 0)
							{
								$getlessiondetail[] = $result;
								ResponseClass::successResponseInArray("AllDetails",$getlessiondetail,"1","Successfully Response","True");
							}
							else
							{
								ResponseClass::ResponseMessage("9","Does Not Match!","False");
							}
							
						}
				}
				
			}
			else
			{
					
				ResponseClass::ResponseMessage("2","Does Not Match!","False");
			}
	
	}else{
				ResponseClass::ResponseMessage("7", "Location not found", "False");
		 }
	
	}

	else

	{

		ResponseClass::ResponseMessage("4","Something Went Wrong","False");

	}
	
		
		
?>