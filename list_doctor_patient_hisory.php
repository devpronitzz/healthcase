<?php 
// turn on output buffering 
ob_start();

    error_reporting(0);
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');
	//error_reporting( E_ALL & ~E_DEPRECATED & ~E_NOTICE );
	
	
	include_once 'connection/db.php';
	include_once 'publicClass/responseClass.php'; 


    define('DB_DRIVER', 'mysql');
	define("DB_HOST", $Mdb_host);
	define("DB_USER", $Mdb_username);
	define("DB_PASSWORD", $Mdb_password);
	define("DB_DATABASE", $Mdb_name);
 
// basic options for PDO 
$dboptions = array(
    PDO::ATTR_PERSISTENT => FALSE,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
	PDO::MYSQL_ATTR_INIT_COMMAND => 'SET sql_mode="NO_ENGINE_SUBSTITUTION"'
);
 
//connect with the server
try {
    $DB = new PDO(DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD, $dboptions);
} catch (Exception $ex) {
    echo($ex->getMessage());
    die;
}

if(isset($_GET['member_id']) && !empty($_GET['member_id'])){
	
	$member_id = $_GET['member_id'];
	$sql = "SELECT * FROM tbl_member WHERE id = '$member_id' AND action = 'Enable'";
	
}

else
{
	$sql = "SELECT * FROM tbl_member WHERE action = 'Enable'";
}


		try {
			$stmt = $DB->prepare($sql);
			$stmt->execute();
			$results = $stmt->fetchAll();
			}
		

		catch (Exception $ex) {
			printErrorMessage($ex->getMessage());
		}

		$returnArray = array();
		$returnArray2 = array();
		
		if (count($results) > 0) {
			
			 $returnArray['success'] = true;
			 $returnArray['message'] = "data found";
	    	 $returnArray['code'] = 200;
			
			 
			 foreach ($results as $rs) {
				
				$start_date1 = $rs['start_date'];
				
				if($start_date1 == '0000-00-00'){
					$start_date = '0000-00-00';
				}else{
					$start_date = date('Y-m-d', strtotime($start_date1));
				}
				
				$end_date1 = $rs['end_date'];
				if($end_date1 == '0000-00-00'){
					$end_date = '0000-00-00';
				}else{
					$end_date = date('Y-m-d', strtotime($end_date1));
				}
				
				
				$image_name = $rs['image_name'];
				
				if($image_name == ""){
					$profile_image_url1 = "";
				}else{
					$profile_image_url1 = $profile_image_url.$rs['image'];	
				}
				
				$package_id = $rs['package_id'];
				$sql1 = "SELECT * FROM tbl_package where id = '$package_id'";
				$row1 = $DB->prepare($sql1);
				$row1->execute();
				$fetch1 = $row1->fetch();
				$package_name1 = $fetch1['package_name'];
				
				
				if($package_name1 == ""){
					$package_name = null;
				}else{
					$package_name = $package_name1;	
				}
				
						$rsFormatted['member_id'] = $rs['id'];
						$rsFormatted['first_name'] = $rs['first_name'];
						$rsFormatted['last_name'] = $rs['last_name'];
						$rsFormatted['user_name'] = $rs['user_name'];
						$rsFormatted['mobile_number'] = $rs['mo_number'];
						$rsFormatted['email_address'] = $rs['email_address'];
						$rsFormatted['password'] = $rs['password'];
						$rsFormatted['mac_address'] = $rs['mac_address'];
						$rsFormatted['package_name'] = $package_name;
						$rsFormatted['image'] = $profile_image_url1;
						$rsFormatted['type'] = $rs['type'];
						$rsFormatted['start_date'] = $start_date;
						$rsFormatted['end_date'] = $end_date;
						$rsFormatted['action'] = $rs['action'];
						if(isset($_GET['member_id'])){
						$returnArray2 = $rsFormatted;
						}else{
						$returnArray2[] = $rsFormatted;
						}
			}
				
				 $returnArray['result'] = $returnArray2;
		}
		else {
					$returnArray['success'] = false;
	   				$returnArray['message'] = "data not found";
	    			$returnArray['code'] = 101;
					$returnArray['result'] = $returnArray2;
					
				}
		

echo json_encode($returnArray, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);


?>

