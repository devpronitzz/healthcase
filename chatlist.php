<?php	
session_start();
	include('classes/mainclass.php');
	header('Content-type: application/json; charset=utf-8');
	$CustomHeaders = apache_request_headers();
	
		$temps = array();
		$chatdetail = array();
		$doctorchatlist = array();
		$pharmacyarray = array();
		$chatdoctordetail = array();
		$chatpatientdetail = array();
		$chatpharmacydetail = array();
		
	if(!empty($_POST['id']) and isset($_POST['id']))
	{
		$id = $_POST['id'];
		$type = $_POST['type'];
		
		$dataObj = new UserClass();
		
	
		if($type == 'D')
		{
					$tempd = array();
					$getgrouplist = $dataObj->get_group_list($id);
					while($resultdetails = mysql_fetch_assoc($getgrouplist))
					{
						$tempd = "";
						$group_id = $resultdetails['id'];
						$countunreadmessage = $dataObj->count_unread_group_message($group_id,$id);
						$resultdetails['unread'] = mysql_num_rows($countunreadmessage);
						
						$getmessagedet = $dataObj->getlastmessagegroup($group_id);
						if(mysql_num_rows($getmessagedet) > 0)
						{
							 $resultlastmessage = mysql_fetch_assoc($getmessagedet);
							 $tempd[] = $resultlastmessage;
							 $resultdetails['message'] = $tempd;
						}
						else
						{
							$resultdetails['message'] ="";
						}
						
							
						$temps[] = $resultdetails;
					}
					
					
					$get_chat_patient = $dataObj->get_chat_patient_details($id,$type);
					
					$count_patient = count($get_chat_patient);
					if($count_patient > 0) {
						
					for($pc=0;$pc<$count_patient;$pc++) 
					{
						$patient_idd = $get_chat_patient[$pc];
						
						$getpatientdetail = $dataObj->getpatientdetails($patient_idd);
					
						$temp = array();
						
						$result = mysql_fetch_assoc($getpatientdetail);
						
						$PtTookStatus = "0";
						
						if($dataObj->getPatientTookStatus($patient_idd, $id) != "")
						{
						    $PtTookStatus = "1";
						}
						
							$temp = "";
							$tem = array();
						
							$res = $result['id'];
							
							$rowdetail['id'] = $result['id'];
							$rowdetail['PtTookStatus'] = $PtTookStatus;
							$rowdetail['name'] = $result['name'];
							$rowdetail['profile_pic'] = $result['profile_pic'];
							$rowdetail['type'] = $result['type'];
							
							$getmessage = $dataObj->getmessaged($id,$res);
							if(mysql_num_rows($getmessage) > 0)
							{
								$msgresult = mysql_fetch_assoc($getmessage);
							
								$countunreadmessage = $dataObj->count_unread_pat_message($id,$res,$type);
								$rowdetail['unread'] = mysql_num_rows($countunreadmessage);
								
								//$result['unread'] = $unreadmsg;
								$temp[] = $msgresult;
								$rowdetail['message'] = $temp;
						
							
							}
							else
							{
								$rowdetail['unread'] = 0;
								$rowdetail['message'] ="";
							}
							
						
						$chatdetail[] = $rowdetail;
						
						
					}
					
					}
					
					$getPharmacy = $dataObj->get_chat_pharmacy_details($id,$type);
					$countPharmacy = count($getPharmacy);
					if($countPharmacy > 0) {
						
						for($df=0;$df<$countPharmacy;$df++) 
						  
						  {
							
							$pharmacyID = $getPharmacy[$df];
							
							//if($pharmacyrow['from_id'] != $id) {
								
								$get_pharmacyD = $dataObj->get_pharmacy_details_api_d($pharmacyID);
								$phar_detail = mysql_fetch_assoc($get_pharmacyD);
								$tempfdd = "";
								$fid = $phar_detail['pharmacy_id'];
								$pharmacyrow['id'] = $phar_detail['pharmacy_id'];
								$pharmacyrow['PtTookStatus'] = 0;
								$pharmacyrow['name'] = $phar_detail['pharmacy_name'];
								$pharmacyrow['profile_pic'] = $phar_detail['profile_pic'];
								$pharmacyrow['type'] = $phar_detail['type'];
								$getpharmacymessage = $dataObj->getpharmacymessaged($id,$fid);
								
								if(mysql_num_rows($getpharmacymessage) != 0)
								{
									$resultlastmessage = mysql_fetch_assoc($getpharmacymessage);
									$tempd = $resultlastmessage;
									$tempfdd[] =  $resultlastmessage;
									$pharmacyrow['message'] = $tempfdd;
									$typedf = 'FD';
									$countunreadmessage = $dataObj->count_unread_far_message($id,$fid,$typedf);
									$pharmacyrow['unread'] = mysql_num_rows($countunreadmessage);
								}
								else
								{
									$pharmacyrow['unread'] = 0;
									$pharmacyrow['message'] ="";
								}
							
							
							
							$pharmacyarray[] = $pharmacyrow;
							
							//}
						}
						
					}
					
					
					
					$get_chat_patient1 = $dataObj->get_chat_doctor_chatlist($id);
					while($doctortodoctor = mysql_fetch_assoc($get_chat_patient1))
					{
						$doctor_Id = $doctortodoctor['to_id'];
						$patientID = $doctortodoctor['from_id'];
						
						if($doctor_Id == $id)
						{
							$get_doctorD1 = $dataObj->getdoctor_details($patientID);
							$temp1 = array();
							
							$result1 = mysql_fetch_assoc($get_doctorD1);
							
								$temp1 = "";
								$diD = $result1['id'];
								
								$doctortodoctor['id'] = $result1['id'];
								
								$doctortodoctor['name'] = $result1['name'];
								$doctortodoctor['PtTookStatus'] = 0;
								$doctortodoctor['profile_pic'] = $result1['profile_pic'];
								$doctortodoctor['type'] = $result1['type'];
								
								$getmessage = $dataObj->getmessaged($id,$diD);
								if(mysql_num_rows($getmessage) > 0)
								{
									$msgresult = mysql_fetch_assoc($getmessage);
									$countunreadmessage = $dataObj->count_unread_message_doctor($id,$doctor_Id,$type);
									$doctortodoctor['unread'] = mysql_num_rows($countunreadmessage);
									$temp1[] = $msgresult;
									$doctortodoctor['message'] = $temp1;
								}
								else
								{
									$doctortodoctor['unread'] = 0;
									$doctortodoctor['message'] = "";
								}
						}
						elseif($patientID == $id)
						{
							$get_doctorD1 = $dataObj->getdoctor_details($doctor_Id);
							$temp1 = array();
							
							$result1 = mysql_fetch_assoc($get_doctorD1);
							
								$temp1 = "";
								$diD = $result1['id'];
								
								$doctortodoctor['id'] = $result1['id'];
								
								$doctortodoctor['name'] = $result1['name'];
								$doctortodoctor['PtTookStatus'] = 0;
								$doctortodoctor['profile_pic'] = $result1['profile_pic'];
								$doctortodoctor['type'] = $result1['type'];
								
								$getmessage = $dataObj->getmessaged($id,$diD);
								if(mysql_num_rows($getmessage) > 0)
								{
									$msgresult = mysql_fetch_assoc($getmessage);
									$countunreadmessage = $dataObj->count_unread_message_doctor($id,$doctor_Id,$type);
									$doctortodoctor['unread'] = mysql_num_rows($countunreadmessage);
									$temp1[] = $msgresult;
									$doctortodoctor['message'] = $temp1;
								}
								else
								{
									$doctortodoctor['unread'] = 0;
									$doctortodoctor['message'] = "";
								}
						}
						$doctorchatlist[] = $doctortodoctor;	
					}
							$rowdetails = array_merge($chatdetail,$temps,$doctorchatlist,$pharmacyarray);
							
							$ord = array();
							foreach ($rowdetails as $key => $value){
							    $ord[] = strtotime($value['message'][0]['date']);
							}
							array_multisort($ord, SORT_DESC, $rowdetails);
					
			/*	foreach($rowdetails as $k=>$v) {
				$sort['message']['date'][$k] = $v['message']['date'];
			
				}
		
				array_multisort($sort['message']['date'], SORT_DESC,$rowdetails);*/
					
				ResponseClass::successResponseInArray("AllDetails",$rowdetails,"1","Successfully Response","True");
			
		}
		elseif($type == 'P')
		{
					$get_chat_doctor = $dataObj->get_chat_doctor_details($id,$type);
					$countDoctor = count($get_chat_doctor);
					
					if($countDoctor > 0) {
						
					for($pd=0;$pd<$countDoctor;$pd++) 
					{
							$d_ID = $get_chat_doctor[$pd];
							
							$get_doctorD = $dataObj->getdoctor_details($d_ID);

								$temp = array();
							
								$result = mysql_fetch_assoc($get_doctorD);
							
								$temp = "";
								$diD = $result['id'];
								
								$row['id'] = $result['id'];
								$row['name'] = $result['name'];
								$row['profile_pic'] = $result['profile_pic'];
								$row['type'] = $result['type'];
								$row['PtTookStatus'] = 0;
								
								$getmessage = $dataObj->getDmessaged($id,$diD);
								if(mysql_num_rows($getmessage) > 0)
								{
									$msgresult = mysql_fetch_assoc($getmessage);
									$countunreadmessage = $dataObj->count_unread_DS_message($diD,$id,$type);
									$row['unread'] = mysql_num_rows($countunreadmessage);
									
									$temp[] = $msgresult;
									$row['message'] = $temp;
								}
								else
								{
									$row['unread'] = 0;
									$row['message'] = "";
								}
								
								$chatdetail[] = $row;
							
							
							
							
					  }
					
					}
					
					$getPharmacy = $dataObj->get_chat_pharmacy_patient_details($id,$type);
					$countPharmacy = count($getPharmacy);
					if($countPharmacy > 0) 
					{
						$tempfdd = array();
						for($pf=0;$pf<$countPharmacy;$pf++)
						{
							$pharmacyID = $getPharmacy[$pf];
							$get_pharmacyD = $dataObj->get_pharmacy_details_api_d($pharmacyID);
							$phar_detail = mysql_fetch_assoc($get_pharmacyD);
							$tempfdd = "";
							$fid = $phar_detail['pharmacy_id'];
							$pharmacyrow['id'] = $phar_detail['pharmacy_id'];
							$pharmacyrow['name'] = $phar_detail['pharmacy_name'];
							$pharmacyrow['profile_pic'] = $phar_detail['profile_pic'];
							$pharmacyrow['type'] = $phar_detail['type'];
							$pharmacyrow['PtTookStatus'] = 0;
							$getpharmacymessage = $dataObj->getpharmacypatientmessaged($id,$fid);
							
							if(mysql_num_rows($getpharmacymessage) != 0)
							{
								$resultlastmessage = mysql_fetch_assoc($getpharmacymessage);
								$tempd = $resultlastmessage;
								$tempfdd[] =  $resultlastmessage;
								$pharmacyrow['message'] = $tempfdd;
								$typedf = 'FP';
								$countunreadmessage = $dataObj->count_unread_far_message($id,$fid,$typedf);
								$pharmacyrow['unread'] = mysql_num_rows($countunreadmessage);
							}
							else
							{
								$pharmacyrow['unread'] = 0;
								$pharmacyrow['message'] ="";
							}
							
							$pharmacyarray[] = $pharmacyrow;
						}
						
					}
					
					/*$getPharmacy = $dataObj->get_pharmacydetail();
					if(mysql_num_rows($getPharmacy) > 0)
					{
						$tempfdd = array();
						while($pharmacyrow = mysql_fetch_assoc($getPharmacy))
						{
							$tempfdd = "";
							$fid = $pharmacyrow['pharmacy_id'];
							$rowdet['id'] = $pharmacyrow['pharmacy_id'];
							$rowdet['name'] = $pharmacyrow['pharmacy_name'];
							$rowdet['profile_pic'] = $pharmacyrow['profile_pic'];
							$rowdet['type'] = $pharmacyrow['type'];
							$getpharmacymessage = $dataObj->getpharmacypatientmessaged($id,$fid);
							
							if(mysql_num_rows($getpharmacymessage) != 0)
							{
								$resultlastmessage = mysql_fetch_assoc($getpharmacymessage);
								$tempd = $resultlastmessage;
								$tempfdd[] =  $resultlastmessage;
								$rowdet['message'] = $tempfdd;
								$typedf = 'FP';
								$countunreadmessage = $dataObj->count_unread_pharmacyd_message($id,$fid,$typedf);
								$rowdet['unread'] = mysql_num_rows($countunreadmessage);
								
							}
							else
							{
								$rowdet['unread'] = 0;
								$rowdet['message'] ="";
							}
							
							$pharmacyarray[] = $rowdet;
						}
					}*/
					
					$rowdetails = array_merge($chatdetail,$pharmacyarray);
							
							$ord = array();
							foreach ($rowdetails as $key => $value){
							    $ord[] = strtotime($value['message'][0]['date']);
							}
							array_multisort($ord, SORT_DESC, $rowdetails);
			ResponseClass::successResponseInArray("AllDetails",$rowdetails,"1","Successfully Response","True");
		}
		else
		{
				$temppatient = array();
				$getpatientID = $dataObj->get_patient_id_chat($id,$type);
				$countPatient= count($getpatientID);
				if($countPatient > 0) {
					
				for($fp=0;$fp<$countPatient;$fp++) 
				{
					$patientID = $getpatientID[$fp];

					$get_chat_patient = $dataObj->get_patient_f($patientID);
					$rowd = mysql_fetch_assoc($get_chat_patient);
					
						$row['id'] = $rowd['id'];
						$row['name'] = $rowd['name'];
						$row['profile_pic'] = $rowd['profile_pic'];
						$row['type'] = $rowd['type'];
						$row['PtTookStatus'] = 0;
						$p_ID = $rowd['id'];
							$temppatient = "";
								$getmessage = $dataObj->getpharmacypatientmessaged1($id,$p_ID);
								if(mysql_num_rows($getmessage) > 0)
								{
									$msgresult = mysql_fetch_assoc($getmessage);
									
									$typedf = 'PF';
									$countunreadmessage = $dataObj->count_pharunread_message($id,$p_ID,$typedf);
									$row['unread'] = mysql_num_rows($countunreadmessage);
									
									$temppatient[] = $msgresult;
									$row['message'] = $temppatient;
								}
								else
								{
									$row['unread'] = 0;
									$row['message'] = "";
								}
								
								$chatdoctordetail[] = $row;
					}
				
				}
				
					
					
					$tempdoctor = array();
					$getdoctorID = $dataObj->get_doctor_id_chat($id,$type);
					$countDoctors= count($getdoctorID);
					
					if($countDoctors > 0) {
						
					for($fd=0;$fd<$countDoctors;$fd++) 
					 {
							$doctID = $getdoctorID[$fd]; 
							
							$get_chat_doctor = $dataObj->get_doctor_f($doctID);
							$doctorrow = mysql_fetch_assoc($get_chat_doctor);
					
								
							$rowdetails['id'] = $doctorrow['id'];
							$rowdetails['name'] = $doctorrow['name'];
							$rowdetails['profile_pic'] = $doctorrow['profile_pic'];
							$rowdetails['type'] = $doctorrow['type'];
							$rowdetails['PtTookStatus'] = 0;
							
							$d_ID = $doctorrow['id'];
							$tempdoctor = "";
								$getdmessage = $dataObj->getpharmacydoctormessaged($id,$d_ID);
								if(mysql_num_rows($getdmessage) != 0)
								{
									$msgdresult = mysql_fetch_assoc($getdmessage);
									
									$typedf = 'DF';
									$countunreadmessage = $dataObj->count_pharunread_message($id,$d_ID,$typedf);
									$rowdetails['unread'] = mysql_num_rows($countunreadmessage);
									
									$tempdoctor[] = $msgdresult;
									$rowdetails['message'] = $tempdoctor;
								}
								else
								{
									$rowdetails['unread'] = 0;
									$rowdetails['message'] = "";
								}			
								$chatpatientdetail[] = $rowdetails;
					   }
					
					}
						
					$tempdoctord = array();
					$getphar = $dataObj->get_pharmacy_det_id();
					while($rowpharmacy = mysql_fetch_assoc($getphar))
					{
							//$doctID = $rowpharmacy['to_id'];
							
							if ($rowpharmacy['pharmacy_id'] !=  $id)
								{
									$rowpharmacy['id'] = $rowpharmacy['pharmacy_id'];
									$rowpharmacy['name'] = $rowpharmacy['pharmacy_name'];
									$rowpharmacy['profile_pic'] = $rowpharmacy['profile_pic'];
									$rowpharmacy['type'] = $rowpharmacy['type'];
									$rowpharmacy['PtTookStatus'] = 0;
									$d_ID = $rowpharmacy['id'];
									$tempdoctord = "";
									$getdmessage = $dataObj->getpharmacydmessaged($id,$d_ID);
									if(mysql_num_rows($getdmessage) != 0)
									{
										$msgdresult = mysql_fetch_assoc($getdmessage);
										$typedf = 'FF';
										$countunreadmessage = $dataObj->count_pharunread_message($id,$d_ID,$typedf);
										$rowpharmacy['unread'] = mysql_num_rows($countunreadmessage);
										
										$tempdoctord[] = $msgdresult;
										$rowpharmacy['message'] = $tempdoctord;
									}
									else
									{
										$rowpharmacy['unread'] = 0;
										$rowpharmacy['message'] = "";
									}			
									$chatpharmacydetail[] = $rowpharmacy;
								}
							
					}
					
					
					
					
					$row_details = array_merge($chatpatientdetail,$chatdoctordetail,$chatpharmacydetail);
							
							$ord = array();
							foreach ($row_details as $key => $value){
							    $ord[] = strtotime($value['message'][0]['date']);
							}
							array_multisort($ord, SORT_DESC, $row_details);
					
					ResponseClass::successResponseInArray("AllDetails",$row_details,"1","Successfully Response","True");		
		}
	}
	else
	{
		ResponseClass::ResponseMessage("4","Something Went Wrong","False");
	}
	
	
		
?>