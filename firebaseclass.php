<?php
$root = $_SERVER['DOCUMENT_ROOT'];
/*
include_once $root.'/services/connection/db.php';
include_once $root.'/services/classes/publicClass/commanClass.php';
include_once $root.'/services/classes/publicClass/responseClass.php';
*/

//include_once 'connection/db.php';
include_once 'publicClass/responseClass.php'; 
require './firebase/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


error_reporting(E_ALL ^ E_DEPRECATED);
//Create Connection Class
class UserClass
{
   	var $conn;
	//constructor to eastablishment connection with database
	public function __construct()
	{
		$serviceAccount = ServiceAccount::fromJsonFile('./firebase/firebase_credentials.json');
		$firebase = (new Factory)
    				->withServiceAccount($serviceAccount)
    				->withDatabaseUri('https://projects-a6962.firebaseio.com')
    				->create();
    	$this->conn = $firebase->getDatabase();			
		
    }
	/*********Query start Following Block 1********/
	
	/*public function imageupload()
	{
		$image_name = $_FILES["fileUpload1"]["name"];	
		$tmp_arr = explode(".",$image_name);
		$img_extn = end($tmp_arr);
		$new_image_name = 'image_'. uniqid() . date('YmdHis').'.'.$img_extn;	
		$flag=0;				 
		if (file_exists("uploads/".$new_image_name))
		{
		   return false;
		}
		else
		{	
			 move_uploaded_file($_FILES["fileUpload1"]["tmp_name"],"uploads/". $new_image_name);
			$flag = 1;
			$static_url = "http://bytesnmaterials.com/healthcare/uploads/".$new_image_name;
			return $static_url; 
		}
	}*/
	
	
	//$imagePath = 'http://bytesnmaterials.com/healthcare/profile/profile.png';
	
	public function signup_doctor($doctorName,$address,$mobileNo,$emailID,$passwordD,$deviceID,$register_image,$latID,$logID,$file_url_name,$imagePath,$unionID1,$feeID,$specialID,$callstatus)
	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");  
		*/
		
			if(isset($_FILES['imageData']['name'])) 
			{	
			if((($_FILES['imageData']['type'] == "image/jpeg")|| ($_FILES['imageData']['type'] == "image/gif")|| ($_FILES['imageData']['type'] == "image/jpg")|| ($_FILES['imageData']['type'] == "image/png")&& ($_FILES['imageData']['size'] < 1000000000000000))){
	
			
	
						if ($_FILES["imageData"]["error"] > 0)        echo "Return Code: " . $_FILES["imageData"]["error"] . "<br />"; else    {
	
							$Ex = end(explode(".",$_FILES['imageData']['name']));
	
							$array = explode(".",$_FILES['imageData']['name']);
	
							$file_url_name = $array[0].time().".".$Ex;
	
							move_uploaded_file($_FILES["imageData"]["tmp_name"], "uploads/col_certificate/".$file_url_name);
			
				}	
			}
		}
		$file_url_name1 = "http://bytesnmaterials.com/healthcare/uploads/col_certificate/".$file_url_name;
		$register_image1 = "http://bytesnmaterials.com/healthcare/uploads/register_image/".$register_image;
		
		/*$sql = "INSERT INTO `doctor_signup` (`device_id`,`register_image`, `doctor_name`, `address`, `mobile_no`, `email_id`, `password`, `lat`, `log`,`col_certificate`, `union_id`, `status`, `type`, `profile_pic`, `specialization`,`payment_id`, `fee`, `chat_status`,`call_status`) VALUES ('$deviceID','$register_image1', '$doctorName', '$address', '$mobileNo', '$emailID', '$passwordD', '$latID', '$logID' ,'$file_url_name1', '$unionID1', '0', 'D','http://bytesnmaterials.com/healthcare/profile/profile.png','$specialID','0','$feeID','0','$callstatus')";
		$result=mysql_query($sql);
		return $result;*/
        
        $postData = [
				'device_id' => $deviceID,
				'register_image' => $register_image1,
				 'doctor_name' => $doctorName,
				 'address' => $address,
				 'mobile_no' => $mobileNo,
				 'email_id' => $emailID,
				 'password' => $passwordD,
				 'lat' => $latID,
				 'log' => $logID,
                'col_certificate' =>$file_url_name1,
                'union_id' => $unionID1,
                'status' => '0',
                'type' => 'D',
                'profile_pic' => 'http://bytesnmaterials.com/healthcare/profile/profile.png',
                'specialization' => $specialID,
                'payment_id' => '0',
                'fee' => $feeID,
                'chat_status' => '0',
                'call_status' => $callstatus,
                'union_id' => '',
                'doctor_id' => '',
                'register_image' => ''
               ];
		
        $postRef = $this->conn->getReference('doctor_signup')->push($postData);
		return $postRef;
        		
	}
	public function insert_notification_details($tokenID,$patientID,$doctorID,$name,$message,$caseD,$date,$type,$status)
	
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `push_notification`(`doctor_id`, `patient_id`, `name`, `message`, `token`, `case_description`,`date`, `type`, `status`) VALUES ('$doctorID', '$patientID','$name','$message','$tokenID','$caseD','$date','P','$status')";
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_push_notification_details($tokenID,$patientID,$patientname,$getresult,$caseD,$date,$type,$status)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		//$sql = "INSERT INTO `push_notification`(`doctor_id`, `patient_id`, `name`, `message`, `token`, `case_description`,`date`, `type`, `status`) VALUES ('$getresult', '$patientID','$name','Request To Chat','$tokenID','$caseD','$date','$type','$status')";
		$sql = "INSERT INTO `chat_message`(`from_id`, `to_id`, `message`, `status`, `type`, `message_type`, `read_status`) VALUES ('$patientID','$getresult','$caseD','0','P','p_text','0')";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_patient_notification($patientID,$doctorID,$message,$date,$status)
	
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'"); 
		$sql = "INSERT INTO `push_notification_details`(`patient_id`, `doctor_id`, `message`, `status`, `date`) VALUES ('$patientID','$doctorID','$message','$status','$date')";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function update_pushnotification($id,$status)
	
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'"); 
		$sql = "UPDATE `push_notification` SET `status`='$status' WHERE `id`='$id'";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function insert_patient_details($patientName,$emailID,$phoneNo,$address,$case_desc,$password,$age,$latitude,$logitude,$type,$tokenID)
	
	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		
		$sql = "INSERT INTO `patient` (`patient_name`, `email_id`, `mobile_no`, `address`,`case_desc`, `profile_pic`, `password`, `age`, `lat`, `log`, `type`, `token`, `took`,`status`,`call_status`) VALUES ('$patientName', '$emailID', '$phoneNo', '$address','$case_desc', 'http://bytesnmaterials.com/healthcare/profile/profile.png', '$password',  '$age', '$latitude', '$logitude', '$type', '$tokenID','0','0','1')";
		
		$result=mysql_query($sql);
		return $result;*/
        $postData = [
            'patient_name' => $patientName,
            'email_id' => $emailID,
            'mobile_no' => $phoneNo,
				'address' => $address,
				'case_desc' => $case_desc,
'profile_pic' => "http://bytesnmaterials.com/healthcare/profile/profile.png",
                'password' => $password,
            'age'=> $age,
                'type' => $type,
                'lat' => $latitude,
                'log' => $logitude,
                'token' => $tokenID,
            'took' => '0',
            'status' => '0',
            'call_status' => '1'
                
               ];
		$postRef = $this->conn->getReference('patient')->push($postData);
	//	print_r($postRef);
	//	exit;
		return $postRef;
	}

	public function insert_pharmacy_details($pharmacyName,$emailID,$phoneNo,$address,$password,$age,$latitude,$logitude,$type,$tokenID,$hour_status,$working_hour)
	
	
	
	{    
	
	if($is_24_hours_support == ""){
		
		$is_24_hours_support1 = "false";
	}else{
		
		$is_24_hours_support1 = $is_24_hours_support;
	}
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `pharmacy`(`pharmacy_name`, `email_id`, `mobile_no`, `address`, `profile_pic`, `password`, `age`, `lat`, `log`, `type`, `token`, `call_status`, `hour_status`, `home_delivery`, `working_hour`, `injection_provider`, `status`) VALUES ('$pharmacyName', '$emailID', '$phoneNo', '$address', 'http://bytesnmaterials.com/healthcare/profile/profile.png', '$password',  '$age', '$latitude', '$logitude', '$type', '$tokenID','1','$hour_status','0','$working_hour','0','0')";
		
		$result=mysql_query($sql);
		return $result;*/
        
        $postData = [
            'pharmacy_name' => $pharmacyName,
            'email_id' => $emailID,
            'mobile_no' => $phoneNo,
				'address' => $address,
'profile_pic' => "http://bytesnmaterials.com/healthcare/profile/profile.png",
                'password' => $password,
            'age'=> $age,
                'type' => $type,
                'lat' => $latitude,
                'log' => $logitude,
                'token' => $tokenID,
            'status' => '0',
            'call_status' => '1',
            'hour_status' => $hour_status,
            'home_delivery' => '0',
            'working_hour' => $working_hour,
            'injection_provider' => '0'
                
               ];
		$postRef = $this->conn->getReference('pharmacy')->push($postData);
		return $postRef;
	}
	
	public function check_patient_mobile_details($mobile,$email_id)
	{
	 //    mysql_query("set names 'utf8'");
		// $sql = "SELECT * FROM  `patient` WHERE mobile_no = '".$mobile."'";	
		// //echo $sql;
		// $result=mysql_query($sql);
		// return $result;

		$check_mobile = $this->conn->getReference('patient')
           			->orderByChild('mobile_no')
   					->equalTo($mobile);
		$snapshot_mobile = $check_mobile->getSnapshot();
		$k1 = $snapshot_mobile->getValue();
		
		$check_email = $this->conn->getReference('patient')
           			->orderByChild('email_id')
   					->equalTo($email_id);
		$snapshot_email = $check_email->getSnapshot();
		$k2 = $snapshot_email->getValue();
		
		
		if(!empty($k1) || !empty($k2)){
		    $k = empty($k1)?$k2:$k1;
		}
		else{
		    $k = [] ;
		}
		
		
		return $k;
	}
	public function insert_quick_registration($mobile_no,$emailId,$password,$latitude,$logitude,$tokenID)
	
	{    
		// mysql_query("set character_set_server='utf8'");
		// mysql_query("set names 'utf8'");
		// $sql = "INSERT INTO `patient` (`mobile_no`, `password`, `type`, `lat`, `log`, `token`) VALUES ('$mobile_no', '$password', 'P', '$latitude', '$logitude','$tokenID')";	
		// //echo $sql;
		// $result=mysql_query($sql);
		// return $result;

		$postData = [
				'address' => "",
				'age' => "",
				 'call_status' => "",
				 'case_desc' => "",
				 'patient_name' => "",
				 'profile_pic' => "",
				 'took' => "",
				 'email_id' => $emailId,
				 'age' => "",
                'status' =>"",
                'password' => $password,
                'type' => 'P',
                'lat' => $latitude,
                'log' => $logitude,
                'token' => $tokenID,
                'patient_id' => "",
                'mobile_no' => $mobile_no
               ];
		$postRef = $this->conn->getReference('patient')->push($postData);
		return $postRef;

	}
	public function get_doctor_details($doctorName,$emailID)

	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from doctor_signup where doctor_name = '$doctorName' and email_id = '$emailID'";
		$result=mysql_query($sql);
		return $result;*/
        $newPost = $this->conn->getReference('doctor_signup')
           			->orderByChild('email_id')
   					->equalTo($emailID);
        $snapshot_unionid = $newPost->getSnapshot();
		$refID = $snapshot_unionid->getValue();
		$getRefKey = array_keys($refID)[0];
        if(count($refID) == 1){
            $data_arr =  array_values($refID);
            $data_arr[0]['doctor_id'] = $getRefKey; 
            return $data_arr;
        }else{
            return [];
        }
	}
	public function get_patient_notification_id($patientID,$doctorID,$date,$tokenID)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from push_notification where doctor_id = '$doctorID' and patient_id = '$patientID' AND date = '$date' AND token = '$tokenID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_patient_lat_log_api($doctorID,$lat,$log)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE doctor_signup SET lat = '$lat', log = '$log' WHERE doctor_id='$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_detail_doctor($toID)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from doctor_signup where doctor_id = '$toID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function check_union_id($unionID)
	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from doctor_signup where union_id = '$unionID'";
		$result=mysql_query($sql);
		return $result;*/
        $newPost = $this->conn->getReference('doctor_signup')
           			->orderByChild('union_id')
   					->equalTo($unionID);
        $snapshot_unionid = $newPost->getSnapshot();
		$refID = $snapshot_unionid->getValue();
        if(count($refID) == 1){
            return $refID;
        }else{
            return [];
        }
	}
	public function check_patient_notification($patientID,$doctorID,$status)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from push_notification_details where doctor_id = '$patientID' and patient_id = '$doctorID' and status = '$status'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_all_patient_details_app($doctorID)	
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT p.* FROM `patient` as p,push_notification_details as ps WHERE ps.doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
		
	public function get_patients_notification_details($patientID,$typeID)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT p.*, pn.status,pn.date,pn.message,pn.id
			FROM push_notification AS pn, patient AS p
			WHERE pn.doctor_id =  '$patientID'
			AND pn.type !=  '$typeID'
			AND pn.patient_id = p.patient_id ORDER BY pn.id DESC";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function get_doctor_notification_details($patientID,$typeID)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT pn.status,pn.date,pn.message,pn.id , d . * 
				FROM  `push_notification` AS pn,  `doctor_signup` AS d
				WHERE pn.patient_id =  '$patientID'
				AND pn.type !=  '$typeID'
				AND d.doctor_id = pn.doctor_id ORDER BY pn.id DESC";
		$result=mysql_query($sql);
		return $result;
	}
	/*-----------------------------------------NOTIFICATION---------------------------------------------------*/
	public function insert_token($tokenID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `push_notification`(`token`) VALUES ('$tokenID')";
		$result=mysql_query($sql);
		return $result;
	}
	public function gettokendetails($tokenID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from push_notification where token = '$tokenID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_patient_name($patientID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from patient where patient_id = '$patientID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_patient_name_det($patientID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select patient_id as id,patient_name as name,profile_pic,type from patient where patient_id = '$patientID'";
		$result=mysql_query($sql);
		return $result;
	}
	
	/*--------------------------------------------------------------------------------------------------------*/
	
	public function update_doctor_profile_details($doctorID,$doctorName,$emailID,$address,$mobileNO,$file_url)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `doctor_signup` SET `doctor_name`='$doctorName',`address`='$address',`mobile_no`='$mobileNO',email_id = '$emailID',`profile_pic`='$file_url' WHERE `doctor_id`='$doctorID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_patient_profile_details($patientID,$patientName,$emailID,$mobileNO,$addressID,$file_url)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `patient` SET `patient_name`='$patientName',`profile_pic`='$file_url',`email_id`='$emailID',`mobile_no`='$mobileNO',`address`='$addressID' WHERE patient_id = '$patientID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_patient_profile($patientID,$patientName,$emailID,$mobileNO,$addressID,$array_full)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		
		$array_full1 = "http://bytesnmaterials.com/healthcare/profile/".$array_full;
		
		$sql = "UPDATE `patient` SET `patient_name`='$patientName',`email_id`='$emailID',`mobile_no`='$mobileNO',`address`='$addressID',`profile_pic`='$array_full1' WHERE patient_id = '$patientID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_pharmacyd_profile($patientID,$patientName,$emailID,$mobileNO,$addressID,$array_full,$lat,$log)
	{    
	
			if($is_24_hours_support == ""){
				
				$is_24_hours_support1 = "false";
			}else{
				
				$is_24_hours_support1 = $is_24_hours_support;
			}
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$array_full1 = "http://bytesnmaterials.com/healthcare/profile/".$array_full;
		
		$sql = "UPDATE `pharmacy` SET `pharmacy_name`='$patientName',`email_id`='$emailID',`mobile_no`='$mobileNO',`address`='$addressID',`profile_pic`='$array_full1',`lat`='$lat',`log`='$log' WHERE pharmacy_id = '$patientID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_phar_profile($patientID,$patientName,$emailID,$mobileNO,$addressID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `pharmacy` SET `pharmacy_name`='$patientName',`email_id`='$emailID',`mobile_no`='$mobileNO',`address`='$addressID' WHERE pharmacy_id = '$patientID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_pharmacy_profile($pharmacyID,$pharmacyName,$emailID,$mobileNO,$addressID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `pharmacy` SET `pharmacy_name`='$pharmacyName',`email_id`='$emailID',`mobile_no`='$mobileNO',`address`='$addressID' WHERE pharmacy_id = '$pharmacyID'" or die(mysql_error());
		$result=mysql_query($sql);
		return $result;
	}
	public function update_doctor_profile($doctorID,$doctorName,$emailID,$feeID,$address,$mobileNO,$array_full)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$array_full1 = "http://bytesnmaterials.com/healthcare/profile/".$array_full;
		$sql = "UPDATE `doctor_signup` SET `doctor_name`='$doctorName',`address`='$address',`mobile_no`='$mobileNO',`email_id`='$emailID',`profile_pic`='$array_full1',`fee`='$feeID' WHERE `doctor_id`='$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function updatedoctortoken($emailID,$password,$token,$latitude,$logitude,$refID)
	{    
		// mysql_query("set character_set_server='utf8'");
		// mysql_query("set names 'utf8'"); 
		// $sql = "UPDATE `doctor_signup` SET `device_id`='$token', lat = '$latitude', log = '$logitude', status = '1' WHERE (`email_id` = '$emailID' OR `mobile_no` = '$emailID') and password = '$password'";
		// $result=mysql_query($sql);
		// return $result;
	$newPost = $this->conn->getReference('doctor_signup')
           			->orderByChild('email_id')
   					->equalTo($emailID);

   	$mobileCheck =  $this->conn->getReference('doctor_signup')
           			->orderByChild('mobile_no')
   					->equalTo($emailID);

		$snapshot_email = $newPost->getSnapshot();
		$refID_email = $snapshot_email->getValue();

		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();

			
		if(count($refID_email) == 1 || count($refID_mobile)==1 ){
			$refIDKey = empty($refID_email) ? $refID_mobile : $refID_email; 
			$refUpdateKey =  empty($refID_email) ? array_keys($refID_mobile)[0] : array_keys($refID_email)[0]; 
			$resetArray =  array_values($refIDKey);

   			if($resetArray[0]['password'] == $password){
   				if(isset($refUpdateKey)) {
   	 $updates = [
    //'doctor_signup/'.$newPostKey => $postData
     'doctor_signup/'.$refUpdateKey.'/device_id' => $token,
     'doctor_signup/'.$refUpdateKey.'/lat' => $latitude,
     'doctor_signup/'.$refUpdateKey.'/log' => $logitude,
     'doctor_signup/'.$refUpdateKey.'/status' => '1',

	];

	$res = $this->conn->getReference() // this is the root reference
   ->update($updates);	
}
else{
	die("update key not found");
}
   	}
   			else{
   				die("password validation failed");
   		}
   	}
   		else{
   			die("no match");
   		}

   	return true;

	}
	public function updatepatienttoken($emailID,$password,$token,$latitude,$logitude)
	{    
		// mysql_query("set character_set_server='utf8'");
		// mysql_query("set names 'utf8'"); 
		// $sql = "UPDATE `patient` SET `token`='$token', lat = '$latitude', log = '$logitude', status = '1' WHERE (`email_id` = '$emailID' OR `mobile_no` = '$emailID') and `password` = '$password'";
		// $result=mysql_query($sql);
		// return $result;

		$newPost = $this->conn->getReference('patient')
           			->orderByChild('email_id')
   					->equalTo($emailID);

   	   $mobileCheck =  $this->conn->getReference('patient')
           			->orderByChild('mobile_no')
   					->equalTo($emailID);

		$snapshot_email = $newPost->getSnapshot();
		$refID_email = $snapshot_email->getValue();

		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();

			
		if(count($refID_email) == 1 || count($refID_mobile)==1 ){
			$refIDKey = empty($refID_email) ? $refID_mobile : $refID_email; 
			$refUpdateKey =  empty($refID_email) ? array_keys($refID_mobile)[0] : array_keys($refID_email)[0]; 
			$resetArray =  array_values($refIDKey);

   			if($resetArray[0]['password'] == $password){
   	if(isset($refUpdateKey)){
   	 $updates = [
    //'doctor_signup/'.$newPostKey => $postData
     'patient/'.$refUpdateKey.'/token' => $token,
     'patient/'.$refUpdateKey.'/lat' => $latitude,
     'patient/'.$refUpdateKey.'/log' => $logitude,
     'patient/'.$refUpdateKey.'/status' => '1'

	];

	$res = $this->conn->getReference() // this is the root reference
   ->update($updates);	
}
else{
	die("update key not found : updatepatienttoken ");
}
   	}
   			else{
   				die("password validation failed");
   		}
   	}
   		else{
   			die("no match");
   		}

   	return true;
	}	
	public function updatepharmacytoken($emailID,$password,$token,$latitude,$logitude)
	{    
		// mysql_query("set character_set_server='utf8'");
		// mysql_query("set names 'utf8'"); 
		// $sql = "UPDATE `pharmacy` SET `token`='$token', status = '1' WHERE (`email_id` = '$emailID' OR `mobile_no` = '$emailID') and `password` = '$password'";
		// $result=mysql_query($sql);
		// return $result;
		$newPost = $this->conn->getReference('pharmacy')
           			->orderByChild('email_id')
   					->equalTo($emailID);

   	   $mobileCheck =  $this->conn->getReference('pharmacy')
           			->orderByChild('mobile_no')
   					->equalTo($emailID);

		$snapshot_email = $newPost->getSnapshot();
		$refID_email = $snapshot_email->getValue();

		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();

			
		if(count($refID_email) == 1 || count($refID_mobile)==1 ){
			$refIDKey = empty($refID_email) ? $refID_mobile : $refID_email; 
			$refUpdateKey =  empty($refID_email) ? array_keys($refID_mobile)[0] : array_keys($refID_email)[0]; 
			$resetArray =  array_values($refIDKey);

   			if($resetArray[0]['password'] == $password){
   	if(isset($refUpdateKey)) {
   	 $updates = [
    //'doctor_signup/'.$newPostKey => $postData
     'pharmacy/'.$refUpdateKey.'/token' => $token,
     'pharmacy/'.$refUpdateKey.'/lat' => $latitude,
     'pharmacy/'.$refUpdateKey.'/log' => $logitude,
     'pharmacy/'.$refUpdateKey.'/status' => '1'

	];

	$res = $this->conn->getReference() // this is the root reference
   ->update($updates);	
}
else{
	die("update key not found");
}
   	}
   			else{
   				die("password validation failed");
   		}
   	}
   		else{
   			die("no match");
   		}

   	return true;




	}
	public function getalldoctordetails()

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from doctor_signup";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_patient_profile_details($patientID)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from patient where patient_id = '$patientID'";
		

		$result=mysql_query($sql);
		return $result;
	}
	public function get_pharmacy_profile_details($pharmacyID)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from pharmacy where pharmacy_id = '$pharmacyID'";
		

		$result=mysql_query($sql);
		return $result;
	}
	public function getalldoctor_details()

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select device_id from doctor_signup";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function checknotificationdetails($patientID,$doctorID)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from push_notification where patient_id = '$patientID' and doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctor_profile_details($doctorID)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from doctor_signup where doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		//$result3 = mysql_fetch_array($sql);
//		$image_old = $result3['profile_pic'];
		return $result;
	}
	
	public function patientdetail($emailID,$phoneNo,$patientName)
	{    
		//mysql_query("set character_set_server='utf8'");
		//mysql_query("set names 'utf8'");
		//$sql = "select patient_id,patient_name,email_id,mobile_no,address,case_description,age,lat,log,type,token from patient where patient_name = '$patientName' and email_id = '$emailID' and mobile_no = '$phoneNo'";
		//$sql = "select * from patient where patient_name = '$patientName' and email_id = '$emailID' and mobile_no = '$phoneNo'";
		//$result=mysql_query($sql);
		//return $result;
        
        $mobileCheck =  $this->conn->getReference('patient')
           			->orderByChild('email_id')
   					->equalTo($emailID);


		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();
        $data_arr = array_values($refID_mobile);
			
		if(count($refID_mobile)==1 ){
		    if($data_arr[0]['mobile_no']==$phoneNo ){
		        $extractKey = array_keys($refID_mobile)[0];
		        $data_arr[0]['patient_id'] = $extractKey;
			return $data_arr; 
			
		    }else{
		        return [];
		    }
   		}
   		else{
   			return [];
   		}
	}
	public function get_pharmacy_detail($emailID,$phoneNo,$pharmacyName)
	{    
// 		mysql_query("set character_set_server='utf8'");
// 		mysql_query("set names 'utf8'");
// 		//$sql = "select patient_id,patient_name,email_id,mobile_no,address,case_description,age,lat,log,type,token from patient where patient_name = '$patientName' and email_id = '$emailID' and mobile_no = '$phoneNo'";
// 		$sql = "select * from pharmacy where pharmacy_name = '$pharmacyName' and email_id = '$emailID' and mobile_no = '$phoneNo'";
// 		$result=mysql_query($sql);
// 		return $result;
        
        $mobileCheck =  $this->conn->getReference('pharmacy')
           			->orderByChild('email_id')
   					->equalTo($emailID);


		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();
        $data_arr =  array_values($refID_mobile);
        if(count($refID_mobile)==1 ){
		    if($data_arr[0]['mobile_no']==$phoneNo){
		    $extractKey = array_keys($refID_mobile)[0];
		    $data_arr[0]['pharmacy_id'] = $extractKey;
		    return $data_arr; 
		    }
		    else{
		        return [];
		    }
   		}
   		else{
   			return [];
   		}
	}
	public function getdoctordetaildetails($emailID,$password)

	{    
		// mysql_query("set character_set_server='utf8'");
		// mysql_query("set names 'utf8'");
		// $sql = "select * from doctor_signup where (email_id = '$emailID' OR mobile_no = '$emailID') and password = '$password'";
		// $result=mysql_query($sql);
		// return $result;

		$newPost = $this->conn->getReference('doctor_signup')
           			->orderByChild('email_id')
   					->equalTo($emailID);

   	$mobileCheck =  $this->conn->getReference('doctor_signup')
           			->orderByChild('mobile_no')
   					->equalTo($emailID);

		$snapshot_email = $newPost->getSnapshot();
		$refID_email = $snapshot_email->getValue();

		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();

			
		if(count($refID_email) == 1 || count($refID_mobile)==1 ){
			$refIDKey = empty($refID_email) ? $refID_mobile : $refID_email; 
			$refKeyU = array_keys($refIDKey)[0];
            $resetArray =  array_values($refIDKey);

   			if($resetArray[0]['password'] == $password){
                $resetArray[0]['doctor_id'] = $refKeyU;
   				return $resetArray[0];
   		
   			}
   			else{
   				die("password validation failed : getdoctordetaildetails");
   			}
   		}
   		else{
   			die("no match: getdoctordetaildetails");
   		}


}
	public function getquickregistrationdetail($mobile_no,$password)

	{    
		// mysql_query("set character_set_server='utf8'");
		// mysql_query("set names 'utf8'");
		// $sql = "select * from patient where mobile_no = '$mobile_no' and password = '$password'";
		// $result=mysql_query($sql);
		// return $result;

   	$mobileCheck =  $this->conn->getReference('patient')
           			->orderByChild('mobile_no')
   					->equalTo($mobile_no);


		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();

			
		if(count($refID_mobile)==1 ){
			$refIDKey =$refID_mobile; 
			$resetArray =  array_values($refIDKey);

   			if($resetArray[0]['password'] == $password){

   				return $refIDKey;
   		
   			}
   			else{
   				die("password validation failed : getquickregistrationdetail");
   			}
   		}
   		else{
   			die("no match: getquickregistrationdetail");
   		}




	}
	
	public function getpatient_details($emailID,$password)

	{    
		// mysql_query("set character_set_server='utf8'");
		// mysql_query("set names 'utf8'");
		// $sql = "select * from patient where (email_id = '$emailID' OR mobile_no = '$emailID') and password = '$password'";
		
		// $result=mysql_query($sql);
		
		// return $result;

		$newPost = $this->conn->getReference('patient')
           			->orderByChild('email_id')
   					->equalTo($emailID);

   	$mobileCheck =  $this->conn->getReference('patient')
           			->orderByChild('mobile_no')
   					->equalTo($emailID);

		$snapshot_email = $newPost->getSnapshot();
		$refID_email = $snapshot_email->getValue();

		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();

			
		if(count($refID_email) == 1 || count($refID_mobile)==1 ){
			$refIDKey = empty($refID_email) ? $refID_mobile : $refID_email;
			$refKeyU = array_keys($refIDKey)[0];
            $resetArray =  array_values($refIDKey);
   			if($resetArray[0]['password'] == $password){
                $resetArray[0]['patient_id'] = $refKeyU;

   				return $resetArray[0];
   		
   			}
   			else{
   				die("password validation failed : getpatient_details");
   			}
   		}
   		else{
   			die("no match: getpatient_details");
   		}




	}
	public function getpharmacy_details($emailID,$password)

	{    
		// mysql_query("set character_set_server='utf8'");
		// mysql_query("set names 'utf8'");
		// $sql = "select * from pharmacy where (email_id = '$emailID' OR mobile_no = '$emailID') and password = '$password'";
		
		// $result=mysql_query($sql);
		// return $result;

		$newPost = $this->conn->getReference('pharmacy')
           			->orderByChild('email_id')
   					->equalTo($emailID);

   	$mobileCheck =  $this->conn->getReference('pharmacy')
           			->orderByChild('mobile_no')
   					->equalTo($emailID);

		$snapshot_email = $newPost->getSnapshot();
		$refID_email = $snapshot_email->getValue();

		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();

			
		if(count($refID_email) == 1 || count($refID_mobile)==1 ){
			$refIDKey = empty($refID_email) ? $refID_mobile : $refID_email;
			$refKeyU = array_keys($refIDKey)[0];
            $resetArray =  array_values($refIDKey);

   			if($resetArray[0]['password'] == $password){
                $resetArray[0]['pharmacy_id'] = $refKeyU;
   				return $resetArray[0];
   		
   			}
   			else{
   				die("password validation failed : getpharmacy_details");
   			}
   		}
   		else{
   			die("no match: getpharmacy_details");
   		}

	}
	public function get_patient_details($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select pa.*,p.*, pa.status as TookStatus from patient_took as pa,patient as p where pa.doctor_id = '$doctorID' and p.patient_id=pa.patient_id and pa.status != 0";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function getMyHistory($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select pa.*,p.*, pa.status as TookStatus from patient_took as pa,doctor_signup as p where pa.time >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND pa.patient_id = '$doctorID' and p.doctor_id=pa.doctor_id";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function getpatientdetailsa($patientID)
	{		
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from patient where patient_id = '$patientID'";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function check_login_details($email,$password,$type)
	{    

	$newPost = $this->conn->getReference('healthcare_login')
           			->orderByChild('email_id')
   					->equalTo($email);

   	$mobileCheck =  $this->conn->getReference('healthcare_login')
           			->orderByChild('mobile_no')
   					->equalTo($email);

		$snapshot_email = $newPost->getSnapshot();
		$refID_email = $snapshot_email->getValue();

		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();

		
		if(count($refID_email) > 0 || count($refID_mobile) > 0 ){
			$refIDKey = empty($refID_email) ? $refID_mobile : $refID_email; 
		$filteredItems = array_filter(	$refIDKey, function($item) use($password,$type){
             return $item['password']==$password && $item['type']==$type;
        });
        
      
   			if(!empty($filteredItems) && count($filteredItems)==1){
  				//login true
   				return $filteredItems;
   	}
   			else{
   				return [];
   		}
   	}
   		else{
   			return [];
   		}



	}
	
	public function checkemaildetails($email)

	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from healthcare_login where email_id = '$emailID'";
		$result=mysql_query($sql);
		return $result;*/
        $newPost = $this->conn->getReference('healthcare_login')
           			->orderByChild('email_id')
   					->equalTo($email);

        $snapshot_email = $newPost->getSnapshot();
		$refID_email = $snapshot_email->getValue();
        
        if(count($refID_email) == 1){
            return $refID_email;
        }
        else{
            return [];
        }
	}
	public function check_mobile_no_s($mobileNo)

	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from healthcare_login where mobile_no = '$mobileNo'";
		
		$result=mysql_query($sql);
		return $result;*/
        
        $mobileCheck =  $this->conn->getReference('healthcare_login')
           			->orderByChild('mobile_no')
   					->equalTo($mobileNo);
        $snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();
        
        if(count($refID_mobile) == 1){
            return $refID_mobile;
        }
        else{
            return [];
        }
        
	}
	
	public function insert_login($dID,$emailID,$mobileNo,$passwordD,$type)
	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `healthcare_login`(`id`,`email_id`, `mobile_no`, `password`, `type`) VALUES 
		('$dID','$emailID','$mobileNo','$passwordD','D')";

		
		$result=mysql_query($sql);
		return $result;*/
        
        $postData = [
                'id' => $dID,
                'email_id' => $emailID,
                'mobile_no' => $mobileNo,
                'password' => $passwordD,
                'type' => 'D'
               ];
		$postRef = $this->conn->getReference('healthcare_login')->push($postData);
		return $postRef;
	}
	public function quick_health_registration($mobileNo,$emailId,$password,$id)
	{    
		// mysql_query("set character_set_server='utf8'");
		// mysql_query("set names 'utf8'");
		// $sql = "INSERT INTO `healthcare_login`(`mobile_no`, `password`, `type`) VALUES ('$mobileNo','$passwordD','P')";
		// $result=mysql_query($sql);
		// return $result;
		$postData = [
                'password' => $password,
                'type' => 'P',
                'mobile_no' => $mobileNo,
                'login_id' => "",
                'id' => $id,
                'email_id' => $emailId
               ];
		$postRef = $this->conn->getReference('healthcare_login')->push($postData);
		return $postRef;
	}
	
	public function insert_p_login($pID,$emailID,$mobile_no,$passwordD)
	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `healthcare_login`(`id`,`email_id`,`mobile_no`, `password`, `type`) VALUES ('$pID','$emailID','$mobile_no','$passwordD','P')";
		$result=mysql_query($sql);
		return $result;*/
        
        $postData = [
                'id' => $pID,
                'email_id' => $emailID,
                'mobile_no' => $mobile_no,
                'password' => $passwordD,
                'type' => 'p'
               ];
		$postRef = $this->conn->getReference('healthcare_login')->push($postData);
		return $postRef;
	}
	public function insert_pharmacy_login($p_ID,$emailID,$mobile_no,$passwordD)
	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `healthcare_login`(`id`,`email_id`,`mobile_no`, `password`, `type`) VALUES ('$p_ID','$emailID','$mobile_no','$passwordD','F')";
		
		$result=mysql_query($sql);
		return $result;*/
        
        $postData = [
                'id' => $p_ID,
                'email_id' => $emailID,
                'mobile_no' => $mobile_no,
                'password' => $passwordD,
                'type' => 'F'
               ];
		$postRef = $this->conn->getReference('healthcare_login')->push($postData);
		return $postRef;
	}
	public function update_status_detail($doctorID,$paymentID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `doctor_signup` SET `payment_id`= '$paymentID',`status`= 1 WHERE `doctor_id`='$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function checkemailid($emailID)

	{    
		/*mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from doctor_signup where email_id = '$emailID'";
		
		$result=mysql_query($sql);
		return $result;*/
        
        $newPost = $this->conn->getReference('doctor_signup')
           			->orderByChild('email_id')
   					->equalTo($emailID);
        $snapshot_emailID = $newPost->getSnapshot();
		$refID = $snapshot_emailID->getValue();
        if(count($refID) == 1){
            return $refID;
        }else{
            return [];
        }
        
	}
	public function check_patient_mobile_no($emailID,$phoneNo)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from patient where email_id = '$emailID' OR mobile_no = '$phoneNo'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function check_doctor_ID($doctorID)

	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from doctor_signup where doctor_id = '$doctorID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	/*------------------------------------------Chat-------------------------------------------------*/
	public function get_type_details($id)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		//$sql = "select * from push_notification_details where doctor_id = '$id' and status = '1'";
		$sql = "select * from chat_message where from_id = '$id' OR to_id = '$id' and type = 'D' Group By to_id";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_last_message_date($fromID,$toID,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from chat_message where to_id = '$toID' and from_id = '$fromID' and type = '$type' ORDER BY chatmsg_id DESC LIMIT 1";
	
		$result=mysql_query($sql);
		return $result;
	}
	public function get_farmacy_last_message_date($fromID,$toid,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from chat_message where to_id = '$toid' and from_id = '$fromID' and type = '$type' ORDER BY chatmsg_id DESC LIMIT 1";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_farmacy_p_last_message_date($fromID,$toid,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from chat_message where to_id = '$toid' and from_id = '$fromID' and type = '$type' ORDER BY chatmsg_id DESC LIMIT 1";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_chat_patient_details($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select from_id,to_id from chat_message where (from_id = '$id' AND type = 'D') OR (to_id = '$id' AND type = 'P')  GROUP BY from_id";
		$result=mysql_query($sql);
		
		
		$PatToID  = array();
		$PatFromID  = array();
		$sqlT = "select DISTINCT to_id from chat_message where (from_id = '$id' AND type = 'D')";
		$qryT=mysql_query($sqlT);
		while($resultT=mysql_fetch_assoc($qryT)) {
			$PatToID[] = $resultT['to_id']; 
		}
		
		$sqlF = "select DISTINCT from_id from chat_message where (to_id = '$id' AND type = 'P')";
		$qryF=mysql_query($sqlF);
		while($resultF=mysql_fetch_assoc($qryF)) {
			$PatFromID[] = $resultF['from_id']; 
		}
		$array_merge_patients =  array_merge($PatToID, $PatFromID);
		$Newpatients = array_unique($array_merge_patients);
		$all_chat_patients = array_values($Newpatients);
		
		/*echo "<pre>";
		print_r($all_chat_patients);
		echo "</pre>";*/
		return $all_chat_patients;
		
	}
	public function get_chat_pharmacy_details($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select from_id,to_id from chat_message where to_id = '$id' AND (type = 'DF' OR type = 'FD') GROUP BY from_id";
		
		$result=mysql_query($sql);
		
		$PhRToID  = array();
		$PhRFromID  = array();
		$sqlT = "select DISTINCT to_id from chat_message where (from_id = '$id' AND type = 'DF')";
		$qryT=mysql_query($sqlT);
		while($resultT=mysql_fetch_assoc($qryT)) {
			$PhRToID[] = $resultT['to_id']; 
		}
		
		$sqlF = "select DISTINCT from_id from chat_message where (to_id = '$id' AND type = 'FD')";
		$qryF=mysql_query($sqlF);
		while($resultF=mysql_fetch_assoc($qryF)) {
			$PhRFromID[] = $resultF['from_id']; 
		}
		$array_merge_pharmcy =  array_merge($PhRToID, $PhRFromID);
		$Newpharmcy = array_unique($array_merge_pharmcy);
		$all_chat_pharmcy = array_values($Newpharmcy);
		
		return $all_chat_pharmcy;
		
		//return $all_chat_pharmcy;
		
	}
	public function get_chat_pharmacy_patient_details($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		/*$sql = "select from_id,to_id from chat_message where from_id = '$id' AND (type = 'FP' OR type = 'PF') GROUP BY to_id";
		
		$result=mysql_query($sql);*/
		
		$PhRToID  = array();
		$PhRFromID  = array();
		$sqlT = "select DISTINCT to_id from chat_message where (from_id = '$id' AND type = 'PF')";
		$qryT=mysql_query($sqlT);
		while($resultT=mysql_fetch_assoc($qryT)) {
			$PhRToID[] = $resultT['to_id']; 
		}
		
		$sqlF = "select DISTINCT from_id from chat_message where (to_id = '$id' AND type = 'FP')";
		$qryF=mysql_query($sqlF);
		while($resultF=mysql_fetch_assoc($qryF)) {
			$PhRFromID[] = $resultF['from_id']; 
		}
		$array_merge_pharmcy =  array_merge($PhRToID, $PhRFromID);
		$Newpharmcy = array_unique($array_merge_pharmcy);
		$all_chat_pharmcy = array_values($Newpharmcy);
		
		
		
		return $all_chat_pharmcy;
		
	}
	public function get_patient_id_chat($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		/*$sql = "select from_id,to_id from chat_message where (from_id = '$id' AND type = 'FP') OR (to_id = '$id' AND type = 'PF') GROUP BY from_id";
		$result=mysql_query($sql);*/
		
		$PatToID  = array();
		$PatFromID  = array();
		$sqlT = "select DISTINCT to_id from chat_message where (from_id = '$id' AND type = 'FP')";
		$qryT=mysql_query($sqlT);
		while($resultT=mysql_fetch_assoc($qryT)) {
			$PatToID[] = $resultT['to_id']; 
		}
		
		$sqlF = "select DISTINCT from_id from chat_message where (to_id = '$id' AND type = 'PF')";
		$qryF=mysql_query($sqlF);
		while($resultF=mysql_fetch_assoc($qryF)) {
			$PatFromID[] = $resultF['from_id']; 
		}
		$array_merge_patients =  array_merge($PatToID, $PatFromID);
		$Newpatients = array_unique($array_merge_patients);
		$all_chat_patients = array_values($Newpatients);
		
		return $all_chat_patients;
	}
	public function get_doctor_id_chat($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		/*$sql = "select from_id,to_id from chat_message where (from_id = '$id' AND type = 'FD') OR (to_id = '$id' AND type = 'DF') GROUP BY to_id";
		
		$result=mysql_query($sql);*/
		
		$DcoToID  = array();
		$DocFromID  = array();
		$sqlT = "select DISTINCT to_id from chat_message where (from_id = '$id' AND type = 'FD')";
		$qryT=mysql_query($sqlT);
		while($resultT=mysql_fetch_assoc($qryT)) {
			$DcoToID[] = $resultT['to_id']; 
		}
		
		$sqlF = "select DISTINCT from_id from chat_message where (to_id = '$id' AND type = 'DF')";
		$qryF=mysql_query($sqlF);
		while($resultF=mysql_fetch_assoc($qryF)) {
			$DocFromID[] = $resultF['from_id']; 
		}
		$array_merge_doctors =  array_merge($DcoToID, $DocFromID);
		$Newdoctors= array_unique($array_merge_doctors);
		$all_chat_doctors = array_values($Newdoctors);
		
		return $all_chat_doctors;
		
	}
	public function get_pharmacy_det_id()
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from pharmacy";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_chat_doctor_chatlist($id)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT from_id,to_id 
				FROM  `chat_message` 
				WHERE (from_id =  '$id'
				OR to_id =  '$id') AND type='DD'
				GROUP BY (
				to_id
				AND from_id
				)";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_chat_doctor_details($id,$type)
	{   
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		/*$sql = "select * from chat_message where from_id = '$id' AND type = '$type' GROUP BY to_id";
		$result=mysql_query($sql);*/
		
		$DocToID  = array();
		$DocFromID  = array();
		$sqlT = "select DISTINCT to_id from chat_message where (from_id = '$id' AND type = 'P')";
		$qryT=mysql_query($sqlT);
		while($resultT=mysql_fetch_assoc($qryT)) {
			$DocToID[] = $resultT['to_id']; 
		}
		
		$sqlF = "select DISTINCT from_id from chat_message where (to_id = '$id' AND type = 'D')";
		$qryF=mysql_query($sqlF);
		while($resultF=mysql_fetch_assoc($qryF)) {
			$DocFromID[] = $resultF['from_id']; 
		}
		$array_merge_doctor =  array_merge($DocToID, $DocFromID);
		$Newdoctor = array_unique($array_merge_doctor);
		$all_chat_doctor = array_values($Newdoctor);
		
		return $all_chat_doctor;
		
	}
	public function get_doctor_requst_details($id)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from push_notification_details where patient_id = '$id'";
		//echo $sql;
		//$sql = "select * from chat_message where from_id = '$id' OR to_id = '$id' and type = 'D' GROUP BY from_id OR to_id";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function getpatientdetails($pID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select patient_id as id,patient_name as name,profile_pic,type from patient where patient_id = '$pID'";
		//echo $sql;
		//$sql = "select patient_id as id,patient_name as name,profile_pic,type from patient";
		$result=mysql_query($sql);
		return $result;
	}
	public function getdoctor_details($pID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select doctor_id as id,doctor_name as name,profile_pic,type from doctor_signup where doctor_id = '$pID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctor_detailrow($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select doctor_id as id,doctor_name as name,profile_pic,type from doctor_signup where doctor_id = '$doctorID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_detail_patient($fromID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from patient where patient_id = '$fromID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function getmessaged($id,$pID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		//$sql = "SELECT chatmsg_id,message,status,date,message_type FROM chat_message WHERE (from_id =  '$id' AND to_id = '$pID') OR (from_id =  '$pID' AND to_id = '$id') ORDER BY chatmsg_id DESC LIMIT 1";
		$sql = "SELECT * FROM chat_message WHERE (from_id = '$id' AND to_id = '$pID' AND type = 'D') OR (from_id =  '$pID' AND to_id = '$id' AND type = 'P') ORDER BY chatmsg_id DESC LIMIT 1";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function getDmessaged($id,$pID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		//$sql = "SELECT chatmsg_id,message,status,date,message_type FROM chat_message WHERE (from_id =  '$id' AND to_id = '$pID') OR (from_id =  '$pID' AND to_id = '$id') ORDER BY chatmsg_id DESC LIMIT 1";
		$sql = "SELECT * FROM chat_message WHERE (from_id = '$id' AND to_id = '$pID' AND type = 'P') OR (from_id =  '$pID' AND to_id = '$id' AND type = 'D') ORDER BY chatmsg_id DESC LIMIT 1";
	
	
		$result=mysql_query($sql);
		return $result;
	}
	
	public function getpharmacymessaged($id,$fID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		//$sql = "SELECT chatmsg_id,message,status,date,message_type FROM chat_message WHERE (from_id =  '$id' AND to_id = '$pID') OR (from_id =  '$pID' AND to_id = '$id') ORDER BY chatmsg_id DESC LIMIT 1";
		$sql = "SELECT * FROM chat_message WHERE (from_id = '$id' AND to_id = '$fID' AND type = 'DF') OR (from_id =  '$fID' AND to_id = '$id' AND type = 'FD') ORDER BY chatmsg_id DESC LIMIT 1";
		
		$result=mysql_query($sql);
		return $result;
	}
	
	public function getpharmacypatientmessaged($id,$fID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");		
		$sql = "SELECT * FROM chat_message WHERE (from_id = '$id' AND to_id = '$fID' AND type = 'PF') OR (from_id =  '$fID' AND to_id = '$id' AND type = 'FP') ORDER BY chatmsg_id DESC LIMIT 1";
		
		$result=mysql_query($sql);
		return $result;
	}
	
	public function getpharmacypatientmessaged1($id,$fID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");		
		$sql = "SELECT * FROM chat_message WHERE (from_id = '$id' AND to_id = '$fID' AND type = 'FP') OR (from_id =  '$fID' AND to_id = '$id' AND type = 'PF') ORDER BY chatmsg_id DESC LIMIT 1";
		
		$result=mysql_query($sql);
		return $result;
	}
	
	public function getpharmacydoctormessaged($id,$fID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM chat_message WHERE (from_id = '$id' AND to_id = '$fID' AND type = 'FD') OR (from_id =  '$fID' AND to_id = '$id' AND type = 'DF') ORDER BY chatmsg_id DESC LIMIT 1";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function getpharmacydmessaged($id,$fID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM chat_message WHERE (from_id = '$id' AND to_id = '$fID' AND type = 'FF') OR (from_id =  '$fID' AND to_id = '$id' AND type = 'FF') ORDER BY chatmsg_id DESC LIMIT 1";
		
		$result=mysql_query($sql);
		return $result;
	}
	
	
	public function insert_message($fromID,$toID,$message,$type,$mtype)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		
		$sql = "INSERT INTO `chat_message`(`from_id`, `to_id`, `message`, `status`, `type`, `message_type`,`read_status`) VALUES ('$fromID','$toID','$message','0','$type','$mtype','0')";
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_invite_doctor_chat($doctorID,$member_ID,$messageID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `chat_message`(`from_id`, `to_id`, `message`, `status`, `type`, `message_type`,`read_status`) VALUES ('$doctorID','$member_ID','$messageID','0','DD','text','0')";
		$result=mysql_query($sql);
		return $result;
	}public function insert_case_description($patientID,$doctorID,$caseD,$date,$type,$status, $FirstLat = '', $FirstLan = '')
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `chat_message`(`from_id`, `to_id`, `message`, `status`, `type`, `message_type`,`read_status`, `first_lat`, `first_lang`) VALUES ('$patientID','$doctorID','$caseD','0','P','p_text','0', '$FirstLat', '$FirstLan')";
		$result=mysql_query($sql);
		return $result;
	}
	public function upload_image($fromID,$toID,$file_url,$type,$mtype)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `chat_message`(`from_id`, `to_id`, `message`, `status`, `type`,`read_status`, `message_type`) VALUES ('$fromID','$toID','$file_url','0','$type','0','$mtype')";		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function upload_image_group($fromID,$toID,$file_url,$type,$mtype)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `group_chat`(`member_id`, `group_id`, `message`, `message_type`, `read_status`, `type`) VALUES ('$fromID','$toID','$file_url','$mtype','0','$type')";		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_message_status($fromID,$toID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE ((`from_id` = '$fromID' AND `to_id` = '$toID') OR (`from_id` = '$toID' AND `to_id` = '$fromID')) AND (type = 'P' OR type = 'D')";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_message_from_pharmacy($fromID,$toID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE ((`from_id` = '$fromID' AND `to_id` = '$toID') OR (`from_id` = '$toID' AND `to_id` = '$fromID')) AND type = 'FP'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_message_far_patient_from_pharmacy($fromID,$toID)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE ((`from_id` = '$fromID' AND `to_id` = '$toID') OR (`from_id` = '$toID' AND `to_id` = '$fromID')) AND type = 'PF'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_message_far_doc_from_pharmacy($fromID,$toID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE ((`from_id` = '$fromID' AND `to_id` = '$toID') OR (`from_id` = '$toID' AND `to_id` = '$fromID')) AND type = 'DF'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_message_phar_to_phar($fromID,$toID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE (`from_id` = '$toID' AND `to_id` = '$fromID') AND type = 'FF'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_message_from_doc_pharmacy($fromID,$toID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE ((`from_id` = '$fromID' AND `to_id` = '$toID') OR (`from_id` = '$toID' AND `to_id` = '$fromID')) AND type = 'FD'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_patientd_message_from_pharmacy($fromID,$toID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE ((`from_id` = '$fromID' AND `to_id` = '$toID') OR (`from_id` = '$toID' AND `to_id` = '$fromID')) AND type = 'PF'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_docmessage_from_pharmacy($fromID,$toID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE ((`from_id` = '$fromID' AND `to_id` = '$toID') OR (`from_id` = '$toID' AND `to_id` = '$fromID')) AND type = 'DF'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_messageFF_from_pharmacy($fromID,$toID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE ((`from_id` = '$fromID' AND `to_id` = '$toID') OR (`from_id` = '$toID' AND `to_id` = '$fromID')) AND type = 'FD'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_messagefp_from_pharmacy($fromID,$toID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `chat_message` SET `read_status`='1' WHERE ((`from_id` = '$fromID' AND `to_id` = '$toID') OR (`from_id` = '$toID' AND `to_id` = '$fromID')) AND type = 'FP'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_read_message_from_group($toID,$fromID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `readchatmessage` SET `status`='1' WHERE group_id = '$toID' AND member_id = '$fromID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_chat_image($fromID,$toID,$type,$mtype)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM chat_message WHERE patient_id =  '$pid'";
		$result=mysql_query($sql);
		return $result;
	}
	public function getmessagedetail($fid,$tid,$type,$to_type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT chatmsg_id,from_id,to_id,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' AND to_id = '$tid' AND type = 'D') OR (from_id = '$tid' and to_id = '$fid' AND type = 'P')";
		$result=mysql_query($sql);
		return $result;
	}
	public function getfmessagedetail($fid,$tid,$type,$to_type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT chatmsg_id,from_id,to_id,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' AND to_id = '$tid' AND type = 'DF') OR (from_id = '$tid' AND to_id = '$fid' AND type = 'FD')";
		$result=mysql_query($sql);
		return $result;
	}
	public function getpatientmessagedetail($fid,$tid,$type,$to_type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT from_id,to_id,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' AND to_id = '$tid' AND type = 'PF') OR (from_id = '$tid' AND to_id = '$fid' AND type = 'FP')";
		$result=mysql_query($sql);
		return $result;
	}
	public function getmessagedetaildetails($fid,$tid,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT from_id,to_id,message,status,date,type,message_type from `chat_message` where ((from_id = '$fid' and to_id = '$tid') OR (from_id = '$tid' and to_id = '$fid')) AND type='DD'";
		$result=mysql_query($sql);
		return $result;
	}
	public function getpmessagedetail($fid,$tid,$type,$to_type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT from_id,to_id,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' AND to_id = '$tid' AND type = 'P') OR (from_id = '$tid' AND to_id = '$fid' AND type = 'D')";
		//$sql = "SELECT from_id as toid,to_id as fromid,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' and to_id = '$tid') OR (from_id = '$tid' and to_id = '$fid')";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_pharmacy_chat_message($fid,$tid,$type,$to_type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT chatmsg_id,from_id,to_id,message,status,date,type,message_type from `chat_message` where ((from_id = '$fid' and to_id = '$tid') OR (from_id = '$tid' and to_id = '$fid')) AND (type = 'DF' OR type = 'PF' OR type = 'FD' OR type = 'FP' OR type = 'FF')";
		//$sql = "SELECT from_id as toid,to_id as fromid,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' and to_id = '$tid') OR (from_id = '$tid' and to_id = '$fid')";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function get_pharmacy_doctor_chat_message($fid,$tid,$type,$to_type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT chatmsg_id,from_id,to_id,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' AND to_id = '$tid' AND type = 'FD') OR (from_id = '$tid' AND to_id = '$fid' AND type = 'DF')";
		//$sql = "SELECT from_id as toid,to_id as fromid,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' and to_id = '$tid') OR (from_id = '$tid' and to_id = '$fid')";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function get_pharmacy_patient_chat_message($fid,$tid,$type,$to_type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT chatmsg_id,from_id,to_id,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' and to_id = '$tid' AND type = 'FP') OR (from_id = '$tid' and to_id = '$fid' AND type = 'PF')";
		//$sql = "SELECT from_id as toid,to_id as fromid,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' and to_id = '$tid') OR (from_id = '$tid' and to_id = '$fid')";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function get_pharmacy_pharmacy_chat_message($fid,$tid,$type,$to_type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT chatmsg_id,from_id,to_id,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' and to_id = '$tid' AND type = 'FF') OR (from_id = '$tid' and to_id = '$fid' AND type = 'FF')";
		//$sql = "SELECT from_id as toid,to_id as fromid,message,status,date,type,message_type from `chat_message` where (from_id = '$fid' and to_id = '$tid') OR (from_id = '$tid' and to_id = '$fid')";
		$result=mysql_query($sql);
		return $result;
	}
	
	
	
	public function count_unread_message($id,$res,$type)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chat_message` WHERE (`from_id` = '$res' AND `to_id` = '$id') and `read_status` = '0' AND type != '$type'";
		$result=mysql_query($sql);
		return $result;
	}
	public function count_unread_pharmacyd_message($id,$res,$type)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chat_message` WHERE (`from_id` = '$id' AND `to_id` = '$res') and `read_status` = '0' AND type = 'FP'";
		$result=mysql_query($sql);
		return $result;
	}
	public function count_unread_far_message($id,$res,$type)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chat_message` WHERE (`from_id` = '$res' AND `to_id` = '$id') and `read_status` = '0' AND type = 'FD'";
		$result=mysql_query($sql);
		return $result;
	}
	public function count_unread_DS_message($id,$res,$type)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chat_message` WHERE (`from_id` = '$res' AND `to_id` = '$id') and `read_status` = '0' AND type = 'D'";
		$result=mysql_query($sql);
		return $result;
	}
	public function count_pharmacyunread_message($id,$res,$type)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chat_message` WHERE (`from_id` = '$res' AND `to_id` = '$id') and `read_status` = '0' AND type = 'FP'";
		$result=mysql_query($sql);
		return $result;
	}
	public function count_pharunread_message($id,$res,$type)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chat_message` WHERE (`from_id` = '$res' AND `to_id` = '$id') and `read_status` = '0' AND type = '$type'";
		$result=mysql_query($sql);
		return $result;
	}
	public function count_unread_pat_message($id,$res,$type)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chat_message` WHERE (`from_id` = '$res' AND `to_id` = '$id') AND `read_status` = '0' AND type = 'P'";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function count_unread_message_doctor($id,$doctor_Id,$type)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chat_message` WHERE (`from_id` = '$doctor_Id' AND `to_id` = '$id') OR (`from_id` = '$id' AND `to_id` = '$doctor_Id') AND `read_status` = '0'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_member_det($fromID,$toID)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM chatgroup_details WHERE group_id = '$toID' AND doctor_id != '$fromID'";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function count_unread_group_message($group_id,$memberID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `readchatmessage` WHERE group_id = '$group_id' AND member_id = '$memberID' AND `status` = '0'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_unread_message($fromID,$toID,$groupchatID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `readchatmessage`( `groupchat_id`, `group_id`, `member_id`, `status`) VALUES ('$groupchatID','$toID','$fromID','0')";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_patient_took($patientID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE patient set took = '1' where patient_id = '$patientID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_patient_attended($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * 
				FROM  `patient` 
				WHERE patient_id NOT 
				IN (
				SELECT patient_id
				FROM push_notification_details
				WHERE doctor_id =  '$doctorID'
				AND STATUS =  '1'
				)";
				
		$result=mysql_query($sql);
		return $result;
	}
	
	public function get_doctor_token_details($getresult,$categoryID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT device_id 
				FROM  `doctor_signup`
				WHERE doctor_id = '$getresult'";
				
		$result=mysql_query($sql);
		return $result;
	}
	
	
	public function get_patient_token_details($PatientId)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT token FROM  `patient` WHERE patient_id = '$PatientId'";
		$result=mysql_fetch_assoc(mysql_query($sql));
		return $result;
	}
	
	public function getDoctorTokenDevice($PatientId)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT device_id FROM  `doctor_signup` WHERE doctor_id = '$PatientId'";
		$result=mysql_fetch_assoc(mysql_query($sql));
		return $result;
	}
	
	public function get_pharmacydetail()
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from pharmacy";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_pharmacy_details_api($fromID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from pharmacy";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_pharmacy_details_api_d($fromID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from pharmacy where pharmacy_id = '$fromID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_patient_f($patientID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT `patient_id` as id, `patient_name` as name, `profile_pic`, `type` FROM `patient` where patient_id = '$patientID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctor_f($dID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT `doctor_id` as id, `doctor_name` as name, `profile_pic`, `type` FROM `doctor_signup` where doctor_id = '$dID'";
		$result=mysql_query($sql);
		return $result;
	}
	
	/*------------------------------------------End Chat-------------------------------------------------*/
	
	/*------------------------------------------Group Chat-------------------------------------------------*/

	public function create_group($groupName,$doctorID,$groupImg)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `create_group`(`group_name`, `doctor_id`,`group_image`,`type`) VALUES ('$groupName',$doctorID,'$groupImg','G')";		
		$result=mysql_query($sql);
		return $result;
	}
	
	public function get_group_records($groupName,$doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from `create_group` where group_name = '$groupName' AND doctor_id = '$doctorID'";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	
	public function get_patient_help($patient_id,$speciallize_doctor,$description)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from `patient_help`";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	
		public function create_group_without_url($groupName,$doctorID,$file_url_name)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		
		if(isset($_FILES['imageData']['name'])) 
			{	
			if((($_FILES['imageData']['type'] == "image/jpeg")|| ($_FILES['imageData']['type'] == "image/gif")|| ($_FILES['imageData']['type'] == "image/jpg")|| ($_FILES['imageData']['type'] == "image/png")&& ($_FILES['imageData']['size'] < 1000000000000000))){
	
			
	
						if ($_FILES["imageData"]["error"] > 0)        echo "Return Code: " . $_FILES["imageData"]["error"] . "<br />"; else    {
	
							$Ex = end(explode(".",$_FILES['imageData']['name']));
	
							$array = explode(".",$_FILES['imageData']['name']);
	
							$file_url_name = $array[0].time().".".$Ex;
	
							move_uploaded_file($_FILES["imageData"]["tmp_name"], "uploads/group_image/".$file_url_name);
			
				}	
			}
		}
		$file_url_name1 = "http://bytesnmaterials.com/healthcare/uploads/group_image/".$file_url_name;
		
		
		$sql = "INSERT INTO `create_group`(`group_name`, `doctor_id`,`group_image`,`type`) VALUES ('$groupName','$doctorID','$file_url_name1','G')";
		$result=mysql_query($sql);
		return $result;
	}
	public function add_doctor_from_group($groupName,$doctorID,$member_id)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `chatgroup_details`(`group_id`, `doctor_id`,`member_id`) VALUES ('$groupName','$doctorID','$member_id')";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function add_patient_help($patient_id,$speciallize_doctor,$description)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `patient_help`(`patient_id`, `speciallize_doctor`, `description`) VALUES ('$patient_id','$speciallize_doctor','$description')";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function get_group_all_doctor($groupID,$doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chatgroup_details` where group_id = '$groupID' AND doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_all_group_doctor($doctor_iD)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM doctor_signup where doctor_id != '$doctor_iD'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_group_list($id)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT g.group_id AS id, g.group_name AS name, g.group_image AS profile_pic, g.type
		FROM  `create_group` AS g, chatgroup_details AS cg
		WHERE cg.group_id
		IN (
		SELECT group_id
		FROM chatgroup_details
		WHERE cg.doctor_id =  '$id'
		) AND g.group_id = cg.group_id";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_group_name($groupName)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `create_group` where group_id = '$groupName'";
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_group_admin_details($groupId,$doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `chatgroup_details`(`group_id`, `doctor_id`, `member_id`) VALUES ('$groupId','$doctorID','$doctorID')";
		
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function get_group_admin_id($groupID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `create_group` where group_id = '$groupID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function getlastmessagegroup($group_id)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `group_chat` where group_id = '$group_id' ORDER BY groupchat_id DESC LIMIT 1";
		$result=mysql_query($sql);
		return $result;
	}
		public function getgroupmessagedetail($tid,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		
		$sql = "SELECT groupchat_id,member_id as fromid,group_id,message,message_type,read_status,type,date FROM `group_chat` where group_id = '$tid' AND type = '$type'";
		$result=mysql_query($sql);
		return $result;
	}
		public function get_doctor_profile_pic($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT profile_pic FROM `doctor_signup` where doctor_id = '$doctorID'";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function getgroup_doc_IDet($id)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chatgroup_details` where doctor_id = '$id'";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_group_message($fromID,$toID,$message,$type,$mtype)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `group_chat`(`member_id`, `group_id`, `message`, `message_type`, `read_status`, `type`) VALUES ('$fromID','$toID','$message','$mtype',0,'$type')";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_all_doctor_group_details($groupID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `chatgroup_details` where group_id = '$groupID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_group_doctor_token_detail($docID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT device_id FROM `doctor_signup` where doctor_id = '$docID'";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function get_last_group_message_date($toID,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `group_chat` where group_id = '$toID' ORDER BY groupchat_id DESC LIMIT 1";
		$result=mysql_query($sql);
		return $result;
	}
	public function check_pstient_took($patientID,$doctorID)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `patient_took` where doctor_id = '$doctorID' AND patient_id = '$patientID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function check_patienttook($patientID,$doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `patient_took` where doctor_id = '$doctorID' AND patient_id = '$patientID' AND status = '1'";
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_patient_took($patientID,$doctorID,$status,$duration, $lat, $lang)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		//$sql = "SELECT * FROM `patient_took` where patient_id = '$patientID' AND doctor_id = '$doctorID' AND doctor_id = '$status'";
		$sql = "INSERT INTO `patient_took`(`patient_id`, `doctor_id`, `status`,`duration`, `lat`, `lang`) VALUES ('$patientID','$doctorID','$status','$duration', '$lat', '$lang')";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_took_patientd($patientID,$doctorID,$status)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		//$sql = "SELECT * FROM `patient_took` where patient_id = '$patientID' AND doctor_id = '$doctorID' AND doctor_id = '$status'";
		$sql = "UPDATE `patient_took` set `status` = '$status' Where patient_id = '$patientID' AND doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function update_took_patientdAndTime($patientID,$doctorID,$status, $Duration)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		//$sql = "SELECT * FROM `patient_took` where patient_id = '$patientID' AND doctor_id = '$doctorID' AND doctor_id = '$status'";
		$sql = "UPDATE `patient_took` set `status` = '$status', `duration` = '$Duration' Where patient_id = '$patientID' AND doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function get_doctor_name_notification($doctorID)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		//$sql = "SELECT * FROM `patient_took` where patient_id = '$patientID' AND doctor_id = '$doctorID' AND doctor_id = '$status'";
		$sql = "SELECT * from doctor_signup where doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctor_deviceId_D($getresult)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		
		$sql = "SELECT * from doctor_signup where doctor_id = '$getresult'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_patient_ID($doctorID)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from patient_took where doctor_id = '$doctorID' AND status = '1'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctorID($id)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from patient_took where patient_id = '$id' AND status = '1'";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function check_mail_id($email)
	{
        
    $newPost = $this->conn->getReference('healthcare_login')
           			->orderByChild('email_id')
   					->equalTo($email);

   	$mobileCheck =  $this->conn->getReference('healthcare_login')
           			->orderByChild('mobile_no')
   					->equalTo($email);

		$snapshot_email = $newPost->getSnapshot();
		$refID_email = $snapshot_email->getValue();

		$snapshot_mobile = $mobileCheck->getSnapshot();
		$refID_mobile = $snapshot_mobile->getValue();
			
		if(count($refID_email) == 1 || count($refID_mobile) == 1 ){
			$refIDKey = empty($refID_email) ? $refID_mobile : $refID_email; 
			$resetArray =  array_values($refIDKey);

            return $refIDKey;
        }
        else{
            return [];
        }
        
	}
	public function update_healthcare_login($id,$npassword)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "Update healthcare_login SET password = '$npassword' where (email_id = '$id' OR mobile_no = '$id')";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_patient_password_details($id,$npassword)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `patient` SET `password`='$npassword' WHERE email_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_doctor_password_details($id,$npassword)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `doctor_signup` SET `password`='$npassword' WHERE email_id = '$id'";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function update_patient_mpassword_details($id,$npassword)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `patient` SET `password`='$npassword' WHERE mobile_no = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_doctor_mpassword_details($id,$npassword)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `doctor_signup` SET `password`='$npassword' WHERE mobile_no = '$id'";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}

	public function update_doctor_call_status($id,$type,$status)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `doctor_signup` SET call_status = '$status' WHERE `doctor_id`='$id'";
		//echo $sql;
		$result=mysql_query($sql);
		return $result;
	}
	public function update_patient_call_status($id,$type,$status)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `patient` SET call_status = '$status' WHERE patient_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctor_call_status($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from doctor_signup where doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}

	public function update_patient_logout_status($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE patient SET token = '', status = '2' where patient_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_doctor_logout_status($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE doctor_signup SET device_id = '', status = '2' where doctor_id = '$id'";
		//$sql = "UPDATE doctor_signup SET  status = '2' where doctor_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_pharmacy_logout_status($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE pharmacy SET token = '', status = '2' where pharmacy_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	
	
	
	public function update_doctor_status($id,$status)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE doctor_signup SET status = '$status' where doctor_id = '$id'";
		//$sql = "UPDATE doctor_signup SET  status = '2' where doctor_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function update_doctor_lat_long($id,$lat,$long)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE doctor_signup SET lat = '$lat', log = '$long' where doctor_id = '$id'";
		//$sql = "UPDATE doctor_signup SET  status = '2' where doctor_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	
	
	public function check_doctor_logout_status($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT status FROM doctor_signup where doctor_id = '$id'";
		$result=mysql_fetch_assoc(mysql_query($sql));
		return $result['status'];
	}
	
	public function update_doctor_took_status($did,$status)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE doctor_signup SET status = '$status' where doctor_id = '$did'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_patient_took_status($pid,$status)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE patient SET status = '$status' where patient_id = '$pid'";

		$result=mysql_query($sql);
		return $result;
	}




	public function compress_image($source_url, $destination_url, $quality) {

		$info = getimagesize($source_url);
		//echo $destination_url;
//echo $destination_url;

    		if ($info['mime'] == 'image/jpeg')
        			$image = imagecreatefromjpeg($source_url);


    		elseif ($info['mime'] == 'image/gif')
        			$image = imagecreatefromgif($source_url);

   		elseif ($info['mime'] == 'image/png')
        			$image = imagecreatefrompng($source_url);
        		//echo $image;
    		imagejpeg($image, $destination_url, $quality);
		return $destination_url;
	}


	/*-----------------------Map All Person API--------------------------------*/

	public function get_all_patient_api()
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from `patient`";
		$result=mysql_query($sql);
		return $result;
	}

	public function get_all_doctor_api($id = "")
	{   
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		
		if($id != "")
		{
			//$sql = "SELECT * from doctor_signup where doctor_id != '".$id."' AND `lat` != '' AND `lat` != '0.0' AND `log` != '' AND `log` != '0.0'";
			$sql = "SELECT * from doctor_signup where `lat` != '' AND `lat` != '0.0' AND `log` != '' AND `log` != '0.0'";
		}
		else{
			$sql = "SELECT * FROM `doctor_signup` WHERE `lat` != '' AND `lat` != '0.0' AND `log` != '' AND `log` != '0.0'";
		}
		$result=mysql_query($sql);
		return $result;
	}
	public function get_all_pharmacy_api()
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from pharmacy";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_patient_api($PatientId)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * from patient where patient_id='$PatientId'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_pharmacy_details($pharmacyID,$hourID,$workinghourID,$home_deliveryID,$inject)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE pharmacy SET hour_status = '$hourID',home_delivery = '$home_deliveryID',working_hour = '$workinghourID',injection_provider = '$inject' where pharmacy_id = '$pharmacyID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_pharmacy_details($pID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM pharmacy where pharmacy_id = '$pID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_healthcare_logindetails($ID,$emailID,$mobileNo,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `healthcare_login` SET `email_id` = '$emailID' , `mobile_no` = '$mobileNo' where `id` = '$ID' AND type = '$type'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function update_healthcare_pharmacy_logindetails($pharmacyID,$emailID,$mobileNO)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE `healthcare_login` SET `email_id` = '$emailID' , `mobile_no` = '$mobileNO' where `id` = '$pharmacyID'";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_lastmessage_groupchat($fromID,$toID,$message)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `group_chat`where `member_id` = '$fromID' AND  `group_id` = '$toID' AND  `message` = '$message' ORDER BY groupchat_id DESC LIMIT 1";
		
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctordetailsD($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `doctor_signup`where doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_rating_details($patientID,$doctorID,$rate)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `rating`(`patient_id`, `doctor_id`, `rate`) VALUES ('$patientID','$doctorID','$rate')";
		$result=mysql_query($sql);
		return $result;
	}
	public function check_rating_status($patientID,$doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM rating where patient_id = '$patientID' AND doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function update_rating_details($patientID,$doctorID,$rate)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "UPDATE rating SET rate = '$rate' where patient_id = '$patientID' AND doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_patient_token_id($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT token as ud_id,patient_id FROM patient where patient_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctor_token_id($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT device_id as ud_id,doctor_id FROM doctor_signup where doctor_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_pharmacy_token_id($id,$type)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT pharmacy_id,token as ud_id FROM pharmacy where pharmacy_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_casedescription_chat_message($getresult,$patientID,$caseD)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT pharmacy_id,token as ud_id FROM pharmacy where pharmacy_id = '$id'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctor_history($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from doctor_signup where doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_doctor_rating($doctorID)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * from rating where doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function get_sum_of_rating($doctorID)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select sum(rate) as rate from rating where doctor_id = '$doctorID'";
		$result=mysql_query($sql);
		return $result;
	}
	public function check_h_took_status($fromID,$toID)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select * FROM patient_took where patient_id = '$fromID' AND doctor_id = '$toID' AND status = 1";
		$result=mysql_query($sql);
		return $result;
	}
	public function insert_duration_chat($patientID,$doctorID,$messg,$status,$mytype)
	{
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "INSERT INTO `chat_message`(`from_id`, `to_id`, `message`, `status`, `type`, `message_type`, `read_status`) VALUES ('$doctorID','$patientID','$messg','$status','$mytype','text','0')";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function getDoctorTookStatus($DocId)
	{
	    mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `patient_took` WHERE `doctor_id` = '$DocId' and `status` = '1'";
		$result=mysql_fetch_assoc(mysql_query($sql));
		return $result;
	}
	
	public function getPatientTookStatus($PTId, $DocId)
	{
	    mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		
		$sql = "SELECT * FROM `patient_took` WHERE `patient_id` = '$PTId' and `doctor_id` = '$DocId' and `status` = '1'";
		$result=mysql_fetch_assoc(mysql_query($sql));
		return $result;
	}
	
	public function GetTookDetail($id)
	{
	    mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT * FROM `patient_took` WHERE `id` = '$id'";
		$result=mysql_fetch_assoc(mysql_query($sql));
		return $result;
	}
	
	public function UpdateDoneStatus($id)
	{
        mysql_query("set character_set_server='utf8'");
        mysql_query("set names 'utf8'");
        $sql = "UPDATE `patient_took` SET status = '2' WHERE `id` = '$id'";
        $result=mysql_query($sql);
        return $result;
	}
	public function UpdatePaitentReview($id, $Rating, $Review)
	{
        mysql_query("set character_set_server='utf8'");
        mysql_query("set names 'utf8'");
        $sql = "UPDATE `patient_took` SET pt_rating = '$Rating', pt_review = '$Review' where `id` = '$id'";
        mysql_query($sql);
	}
	public function UpdateDoctorReview($id, $Rating, $Review)
	{
        mysql_query("set character_set_server='utf8'");
        mysql_query("set names 'utf8'");
        $sql = "UPDATE `patient_took` SET doctor_rate = '$Rating', doctor_review = '$Review' where `id` = '$id'";
        mysql_query($sql);
	}
	
	
	
	public function GetDoctorAvgRating($id)
	{
        mysql_query("set character_set_server='utf8'");
        mysql_query("set names 'utf8'");
        $sql = "SELECT AVG(`doctor_rate`) as AvgRate from patient_took where `doctor_id` = '$id'";
        return mysql_fetch_assoc(mysql_query($sql));
	}
	
	public function get_patient_cancel_details($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select pa.*,p.*, pa.status as TookStatus from patient_took as pa,patient as p where pa.time >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND pa.doctor_id = '$doctorID' and p.patient_id=pa.patient_id and pa.status = 0";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function get_doctor_cancel_details($doctorID)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "select pa.*,p.*, pa.status as TookStatus from patient_took as pa,doctor_signup as p where pa.time >= DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND pa.patient_id = '$doctorID' and p.doctor_id=pa.doctor_id and pa.status = 4";
		$result=mysql_query($sql);
		return $result;
	}
	
	public function AcceptTimeExt($TookId)
	{
	    mysql_query("set character_set_server='utf8'");
        mysql_query("set names 'utf8'");
        $sql = "UPDATE `patient_took` SET status = '1' WHERE `id` = '$TookId'";
        $result=mysql_query($sql);
        return $result;
	}
	public function InsertCancelTook($TookId)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		
		$sql = "SELECT * FROM `patient_took` WHERE `id` = '$TookId'";
		$result=mysql_fetch_assoc(mysql_query($sql));
		
		$PTID = $result['patient_id'];
		$DocId = $result['doctor_id'];
		
		$sql = "INSERT INTO `cancel_took`(`cancel_took_patient`, `cancel_took_doctor`) VALUES ('$PTID','$DocId')";
		mysql_query($sql);
		
		
		$sql = "DELETE FROM `patient_took` WHERE `id` = '$TookId'";
		mysql_query($sql);
	}
	
	public function GetAllPatientRating($id)
	{
        mysql_query("set character_set_server='utf8'");
        mysql_query("set names 'utf8'");
        $sql = "SELECT * from patient_took as ptt LEFT JOIN doctor_signup as DS on ptt.doctor_id=DS.doctor_id where `patient_id` = '$id' and pt_rating != '0'";
        return mysql_query($sql);
	}
	
	public function GetAllDoctorRating($id)
	{
        mysql_query("set character_set_server='utf8'");
        mysql_query("set names 'utf8'");
        $sql = "SELECT * from patient_took as ptt LEFT JOIN patient as DS on ptt.patient_id=DS.patient_id where `doctor_id` = '$id' and doctor_rate != '0'";
        return mysql_query($sql);
	}
	
	/*public function get_doctor_history($doctor_id,$hist_date)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		echo $sql = "SELECT patient_id FROM patient_took where doctor_id = '$doctor_id' AND status = '2' AND time >= '$hist_date'";
		$result=mysql_query($sql);
		return $result;
	}
	*/
	
	/*public function get_doctor_history($doctor_id,$hist_date)
	{    
		mysql_query("set character_set_server='utf8'");
		mysql_query("set names 'utf8'");
		$sql = "SELECT patient_id FROM patient_took where doctor_id = '$doctor_id' AND status = '2' AND time >= '$hist_date'";
		$result=mysql_query($sql);
		return $result;
	}*/
}
?>
