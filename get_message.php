<?php	
session_start();
	include('classes/mainclass.php');
	header('Content-type: application/json; charset=utf-8');
	$CustomHeaders = apache_request_headers();
	$getmesg = array();	
	if(!empty($_POST['fromid']) and isset($_POST['fromid']))
	{
		$fid = $_POST['fromid'];
		$tid = $_POST['toid'];
		$type = $_POST['type'];
		$to_type = $_POST['to_type'];
		$dataObj = new UserClass();
		if($type == 'D')
		{
			$temp = array();
			if($to_type == 'P')
			{
				$result = $dataObj->getmessagedetail($fid,$tid,$type,$to_type);
			}
			elseif($to_type == 'F')
			{
				$result = $dataObj->getfmessagedetail($fid,$tid,$type,$to_type);
			}
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_assoc($result))
				{					
					$rtype = $row['type'];
					$RTYPE = $rtype[0];
					if($rtype == 'P')
					{
						$temp['chatmsg_id'] = $row['chatmsg_id'];
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = $row['type'];
						$temp['message_type'] = $row['message_type'];
					}
					elseif($rtype == 'FD')
					{
						$temp['chatmsg_id'] = $row['chatmsg_id'];
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = 'FD';
						$temp['message_type'] = $row['message_type'];					}
					elseif($rtype == 'D' || $rtype == 'DF')
					{						
						$temp['chatmsg_id'] = $row['chatmsg_id'];
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = 'D';
						$temp['message_type'] = $row['message_type'];
					}
					$getmesg[] = $temp;
				}
				$sort = array();
				foreach($getmesg as $k=>$v) {
				$sort['date'][$k] = $v['date'];
				}
				array_multisort($sort['date'], SORT_ASC,$getmesg);
				ResponseClass::successResponseInArray("AllDetails",$getmesg,"1","Successfully Response","True");
			}
			else
			{
				ResponseClass::ResponseMessage("6","No Record","False");
			}
		}
		elseif($type == 'P')
		{
			$temp = array();
			if($to_type == 'D')
			{
				$result = $dataObj->getpmessagedetail($fid,$tid,$type,$to_type);
			}
			elseif($to_type == 'F')
			{
				$result = $dataObj->getpatientmessagedetail($fid,$tid,$type,$to_type);
			}
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_assoc($result))
				{
					$rtype = $row['type'];
					if($rtype == 'D')
					{
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = $row['type'];
						$temp['message_type'] = $row['message_type'];
					}
					elseif($rtype == 'FP')
					{
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = 'FP';
						$temp['message_type'] = $row['message_type'];
					}
					else
					{
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = 'P';
						$temp['message_type'] = $row['message_type'];
					}
					$getmesg[] = $temp;
				}
				$sort = array();
				foreach($getmesg as $k=>$v) {
				$sort['date'][$k] = $v['date'];
				}
				array_multisort($sort['date'], SORT_ASC,$getmesg);
				ResponseClass::successResponseInArray("AllDetails",$getmesg,"1","Successfully Response","True");
			}
			else
			{
				ResponseClass::ResponseMessage("6","Not success","False");
			}
		}elseif($type == 'G')
		{
			$result = $dataObj->getgroupmessagedetail($tid,$type);
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_assoc($result))
				{
					$doctorID = $row['fromid'];
					$getdoctorpic = $dataObj->get_doctor_profile_pic($doctorID);
					$row['profile_pic'] = mysql_fetch_assoc($getdoctorpic);
					$getmesg[] = $row;
				}
				ResponseClass::successResponseInArray("AllDetails",$getmesg,"1","Successfully Response","True");
			}
			else
			{
				ResponseClass::ResponseMessage("6","Not success","False");
			}
		}
		else
		{
			$temp = array();
			
			if($to_type == 'D')
			{
				$result = $dataObj->get_pharmacy_doctor_chat_message($fid,$tid,$type,$to_type);
			}
			elseif($to_type == 'P')
			{
				$result = $dataObj->get_pharmacy_patient_chat_message($fid,$tid,$type,$to_type);
			}
			else {
					$result = $dataObj->get_pharmacy_pharmacy_chat_message($fid,$tid,$type,$to_type);
				 }
			
			
			/*echo "<pre>";
			print_r(mysql_fetch_assoc($result));
			echo "</pre>";*/
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_assoc($result))
				{	
					$rtype = $row['type'];
					$RTYPE = $rtype[0];
					$chatfrom_id = $row['from_id'];
					if($rtype == 'DF')
					{
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = $RTYPE;
						$temp['message_type'] = $row['message_type'];
					}
					elseif($rtype == 'PF')
					{
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = $RTYPE;
						$temp['message_type'] = $row['message_type'];
					}
					else if($rtype == 'FF')
					{
						if($chatfrom_id == $fid)
						{
							$temp['fromid'] = $row['from_id'];
							$temp['toid'] = $row['to_id'];
							$temp['message'] = $row['message'];
							$temp['status'] = $row['status'];
							$temp['date'] = $row['date'];
							$temp['type'] = $RTYPE;
							$temp['message_type'] = $row['message_type'];
						}
						
						else {
								
								$temp['fromid'] = $row['from_id'];
								$temp['toid'] = $row['to_id'];
								$temp['message'] = $row['message'];
								$temp['status'] = $row['status'];
								$temp['date'] = $row['date'];
								$temp['type'] = $RTYPE;
								$temp['message_type'] = $row['message_type'];
							
							}
					}
					
					else
					{
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = $RTYPE;
						$temp['message_type'] = $row['message_type'];
					}
					
					/*$mainType = $type.$to_type;
					$subType = $to_type.$type;
					$rtype = $row['type'];
					$chatfrom_id = $row['from_id'];
					
					if($mainType == $rtype || $subType == $rtype)
					
					{
					
					if($chatfrom_id == $fid)
					{
						$temp['fromidnew'] = $chatfrom_id;
						$temp['chatid'] = $row['chatmsg_id'];
						$temp['fromidsame'] = "same";
						$temp['fromid'] = $row['from_id'];						
						$temp['toid'] = $row['to_id'];						
						$temp['message'] = $row['message'];						
						$temp['status'] = $row['status'];						
						$temp['date'] = $row['date'];						
						$temp['type'] = $rtype;						
						$temp['message_type'] = $row['message_type'];
					}
					
					else {
							
							$temp['fromidnew'] = $chatfrom_id;
							$temp['chatid'] = $row['chatmsg_id'];
							$temp['fromidsame'] = "not same";
							$temp['fromid'] = $row['from_id'];
							$temp['toid'] = $row['to_id'];
							$temp['message'] = $row['message'];
							$temp['status'] = $row['status'];
							$temp['date'] = $row['date'];
							$temp['type'] = $rtype;
							$temp['message_type'] = $row['message_type'];
						
						}
					}*/
					
					/*if($rtype == 'DF')
					{
						$temp['fromid'] = $row['to_id'];
						$temp['toid'] = $row['from_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = 'D';
						$temp['message_type'] = $row['message_type'];
					}
					elseif($rtype == 'PF')
					{
						$temp['fromid'] = $row['to_id'];
						$temp['toid'] = $row['from_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = 'P';
						$temp['message_type'] = $row['message_type'];
					}
					elseif($rtype == 'FF')	
					{						
						$temp['fromid'] = $row['from_id'];						
						$temp['toid'] = $row['to_id'];						
						$temp['message'] = $row['message'];						
						$temp['status'] = $row['status'];						
						$temp['date'] = $row['date'];						
						$temp['type'] = 'F';						
						$temp['message_type'] = $row['message_type'];																
					}												
				   else
					{
						$temp['fromid'] = $row['from_id'];
						$temp['toid'] = $row['to_id'];
						$temp['message'] = $row['message'];
						$temp['status'] = $row['status'];
						$temp['date'] = $row['date'];
						$temp['type'] = 'F';
						$temp['message_type'] = $row['message_type'];
					}*/
					$getmesg[] = $temp;
				}
				$sort = array();
				foreach($getmesg as $k=>$v) {
				$sort['date'][$k] = $v['date'];
				}
				array_multisort($sort['date'], SORT_ASC,$getmesg);
				ResponseClass::successResponseInArray("AllDetails",$getmesg,"1","Successfully Response","True");
			}
			else
			{
				ResponseClass::ResponseMessage("6","Not success","False");
			}
		}
	}
	else
	{
		ResponseClass::ResponseMessage("7","Something Went Wrong","False");
	}
?>